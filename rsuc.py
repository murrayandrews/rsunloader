#!/usr/bin/env python3

"""

Automated RedShift Extractor With Inline Parameter Expansion - Client for rsud

... aka Redshift Unloader Client (rsuc).

This is a clinet for rsud that generates the SQS messages reuired to initiate
an unload.

-- MA

................................................................................

Copyright (c) 2018, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

# ..............................................................................
# region imports

from __future__ import print_function

import argparse
import json
import logging
import os
import sys

import boto3
import yaml

from common.logging import setup_logging

# endregion imports
# ..............................................................................


# ..............................................................................
# region constants

__author__ = 'Murray Andrews'
__version__ = '1.0.0'

PROG = os.path.splitext(os.path.basename(sys.argv[0]))[0]

LOGNAME = PROG  # Set to None to include boto which uses root logger.
LOG = logging.getLogger(name=LOGNAME)
LOGLEVEL = 'info'  # Default logging level


# endregion constants
# ..............................................................................


# ------------------------------------------------------------------------------
def process_cli_args():
    """
    Process the command line arguments.
    :return:    The args namespace.
    """

    argp = argparse.ArgumentParser(
        prog=PROG,
        description='Redshift table unloader'
    )

    ugrp = argp.add_mutually_exclusive_group(required=True)

    argp.add_argument('-c', '--no-colour', '--no-color', dest='colour', action='store_false',
                      default=True, help='Don\'t use colour in information messages.')

    argp.add_argument('--dry-run', dest='dry_run', action='store_true',
                      help='Do a dry run. The message that would be sent to the'
                           ' queue is shown but not sent.')

    ugrp.add_argument('-f', '--control-file', dest='control_file', action='store',
                      help='Control file from which to read unload specifciations.')

    argp.add_argument('-l', '--level', metavar='LEVEL', default=LOGLEVEL,
                      help='Print messages of a given severity level or above.'
                           ' The standard logging level names are available but info,'
                           ' warning and error are most useful. Default is {}.'.format(LOGLEVEL))

    argp.add_argument('--log', action='store',
                      help='Log to the specified target. This can be either a file'
                           ' name or a syslog facility with an @ prefix (e.g. @local0).')

    argp.add_argument('--profile', action='store', help='As for AWS CLI.')

    argp.add_argument('-q', '--queue', action='store',
                      help='AWS SQS queue name.')

    argp.add_argument('--tag', action='store', default=PROG,
                      help='Tag log entries with the specified value. The default is {}.'.format(PROG))

    argp.add_argument('-v', '--version', action='version', version=__version__,
                      help='Print version and exit.')

    ugrp.add_argument('-u', '--unload', metavar='UPARAM=VALUE', nargs='+',
                      help='Unload specification parameter.')

    return argp.parse_args()


# ------------------------------------------------------------------------------
def key_value_to_dict(plist):
    """
    Convert a list of key=value strings into a dictionary.

    :param plist:       A list of key=value strings.
    :type plist:        list[str]

    :return:            A dictionary version of plist.
    :rtype:             dict[str, str]
    """

    d = {}
    for p in plist:
        try:
            k, v = p.split('=', 1)
        except ValueError:
            raise Exception('Bad key=value: {}'.format(p))
        d[k] = v

    return d


# ------------------------------------------------------------------------------
def get_control_file(control_file):
    """
    Read a control file and return the contents. Does some basic health checking.

    :param control_file:    A YAML rsu style control file.
    :type control_file:     str

    :return:                The control file contents.
    :rtype:                 dict
    """

    with open(control_file) as fp:
        data = yaml.safe_load(fp)

    if not isinstance(data, dict):
        raise Exception('Malformed control file')

    if 'unload' not in data or not isinstance(data['unload'], list):
        raise Exception('Missing or malformed "unload" key')

    return data


# ------------------------------------------------------------------------------
def get_sqs_queue(queue_name, aws_session=None):
    """
    Get the SQS resource for the named queue.

    :param queue_name:  The name or URL of the SQS queue.
    :param aws_session: A boto3 Session instance.
    :
    :type queue_name:   str
    :type aws_session:  boto3.Session

    :return:            The boto3 SQS resource for the queue.
    """

    if not aws_session:
        aws_session = boto3.Session()

    if '.amazonaws.com/' in queue_name:
        # Assume its a URL
        return aws_session.resource('sqs').Queue('https://' + queue_name)
    else:
        return aws_session.resource('sqs').get_queue_by_name(QueueName=queue_name)


# ------------------------------------------------------------------------------
def main():
    """
    Do the business.

    :return:        Status
    :type:          int
    """

    setup_logging(LOGLEVEL, name=LOGNAME, prefix=PROG)
    args = process_cli_args()
    setup_logging(args.level, name=LOGNAME, target=args.log, colour=args.colour, prefix=args.tag)

    # ----------------------------------------
    # Get unload specs from either a control file or the command line
    if args.unload:
        unload_specs = [key_value_to_dict(args.unload)]
    else:
        # Read conteol file
        try:
            control_data = get_control_file(args.control_file)
        except Exception as e:
            raise Exception('{}: {}'.format(args.control_file, e))

        unload_specs = control_data['unload']
        if not args.queue:
            args.queue = control_data.get('queue')

    if not args.queue:
        raise Exception('SQS queue name must be provided')

    # ----------------------------------------
    # Get the SQS queue
    sqs_queue = get_sqs_queue(args.queue, aws_session=boto3.Session(profile_name=args.profile))
    LOG.debug('Found SQS queue %s', sqs_queue.url)

    # ----------------------------------------
    # Send the messages
    for u in unload_specs:
        msg = json.dumps(u, indent=4, sort_keys=True)
        if args.dry_run:
            LOG.info('Message dry run\n%s', msg)
        else:
            LOG.info('Message to %s\n%s', sqs_queue.url, msg)
            sqs_queue.send_message(
                MessageBody=msg
            )


# ------------------------------------------------------------------------------
if __name__ == '__main__':
    # exit(main())  # Uncomment for debugging
    try:
        exit(main())
    except Exception as ex:
        LOG.error('%s', ex)
        exit(1)
    except KeyboardInterrupt:
        LOG.warning('Interrupt')
        exit(2)
