# Installation

[TOC]

**Rsu** itself is pure Python. There are a number of installation options.

## Option 1 - Clone the repo

 > Internet access required

The simplest and preferred method is to clone the repository and install the
required modules in the usual way with `pip`. 

```bash
# Get the code
git clone git@bitbucket.org:murrayandrews/rsunloader.git
cd rsunloader

# Create a virtual environment and install the required modules.
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt --upgrade

# Make sure it runs
chmod u+x rsu.py
./rsu.py --help
```

## Option 2 - Install base code from a tar file

 > Internet access required

If it's not possible to clone the repo to the target machine, an easy alternative is
to install the base code from a tar file.

To create the tar file on a machine with the source already present:

```bash
make rsu
```
This will produce `rsu.tar.bz2`. Copy this to the target machine and run:

```bash
# Assume rsu.tar.bz2 is in the current directory
mkdir rsunloader
cd  rsunloader

# Extract the code
tar xf ../rsu.tar.bz2

# Create a virtual environment and install the required modules.
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt --upgrade

# Make sure it runs
chmod u+x rsu.py
./rsu.py --help
```

## Option 3 - Install base code and non-boto3 modules from a tar file

> Internet access not required but assumes that the target machine already
> has boto3 and its dependencies present. (e.g some EC2 Linux builds).

To create the tar file on a *compatible* machine with the source already present:

```bash
# Make sure all of the required modules are present on source machine
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt --upgrade

# Create the install bundle
make rsu1
```
This will produce `rsu1.tar.bz2` that includes the required non-standard modules
(apart from boto3 and its dependencies). Copy this to the target machine and run:

```bash
# Assume rsu1.tar.bz2 is in the current directory
mkdir rsunloader
cd  rsunloader

# Extract the code
tar xf ../rsu1.tar.bz2

# Make sure it runs. No virtualenv required.
chmod u+x rsu.py
./rsu.py --help
```

## Option 4 - Install base code and all required modules from a tar file

> Internet access not required.

To create the tar file on a *compatible* machine with the source already present:

```bash
# Make sure all of the required modules are present on source machine
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt --upgrade

# Create the install bundle
make rsu2
```

This will produce `rsu2.tar.bz2` that includes all of the required non-standard modules
(including boto3 and its dependencies). Copy this to the target machine and run:

```bash
# Assume rsu2.tar.bz2 is in the current directory
mkdir rsunloader
cd  rsunloader

# Extract the code
tar xf ../rsu2.tar.bz2

# Make sure it runs. No virtualenv required.
chmod u+x rsu.py
./rsu.py --help
```

