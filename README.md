# Redshift Unloader

[TOC]

## Overview

**Rsu** and **rsud** automate the process of unloading data from Redshift to AWS
S3. They are designed to simplify the process of unloading large amounts of
data, either on a scheduled basis or on demand. They are natural counterparts to
[rsDropLoader2](https://bitbucket.org/murrayandrews/evw/src/master/handlers/rsDropLoader2.md)
which automates the loading of data from S3 to one or more Redshift clusters.

Features:

*   Unload from multiple source Redshift clusters.

*   Unload multiple relations (tables / views) in a single run.

*   Multi-threaded to perform unloads in parallel.

*   Include / exclude relations based on regular expressions.

*   Perform customised actions on successful / unsuccessful unload operations.

**Rsu** reads its unload instructions from files and operates in batch mode.

**Rsud** is a daemon variant of **rsu** that receives unload requests on an AWS
SQS queue and runs as a daemon. An associated client, **rsuc**, generates
correctly formatted request messages.

## Installation

Installation instructions can be found [here](INSTALL.md).

## Basic Operation

### rsu

**Rsu** works as follows:

1.  Read one or more YAML formatted [control files](#markdown-header-control-files)
    from the local file system and/or S3. Each control file specifies the
    Redshift cluster and one or more unload operations.

2.  Analyse unload specifications to determine which relations (tables / views)
    are to be unloaded and to which location(s) in S3.

3.  Perform a [basic security check](#markdown-header-s3-bucket-security-checks)
    on the target S3 buckets. Security checks can be disabled using a command
    line option.

4.  Connect to the Redshift cluster to expand / validate the list of relations
    to be unloaded. Connectivity information is obtained from a [DynamoDB
    cluster table](#markdown-header-the-cluster-table).

5.  Pass each unload task to a worker thread to perform the actual
    [UNLOAD](http://docs.aws.amazon.com/redshift/latest/dg/r_UNLOAD.html)
    operation from the Redshift cluster. The number of worker threads is
    configurable using a command line option.

6.  Execute any [post unload actions](#markdown-header-post-unload-actions)
    defined in the unload specification.

7.  Exit once all unloads are completed.

### rsud

**Rsud** works as follows:

1.  Read a YAML formatted [control file](#markdown-header-control-files)
    from the local file system and/or S3. The control file specifies the
    Redshift cluster and the parameters used to control unload operations.

2.  Receive SQS messages specifying which relations (tables / views)
    are to be unloaded. The control file can specify whether the unload location
    in S3 can included in the SQS message or must be obtained from the control
    file.

3.  Perform a [basic security check](#markdown-header-s3-bucket-security-checks)
    on the target S3 buckets. Security checks can be disabled using a command
    line option.

4.  Connect to the Redshift cluster to expand / validate the list of relations
    to be unloaded. Connectivity information is obtained from a [DynamoDB
    cluster table](#markdown-header-the-cluster-table).

5.  Pass each unload task to a worker thread to perform the actual
    [UNLOAD](http://docs.aws.amazon.com/redshift/latest/dg/r_UNLOAD.html)
    operation from the Redshift cluster. The number of worker threads is
    configurable using a command line option.

6.  Execute any [post unload actions](#markdown-header-post-unload-actions)
    defined in the unload specification.

7.  Wait for more unload messages to arrive.

## Control Files

Control files are YAML formatted files that specify:

*   The target Redshift cluster
*   The relations to be unloaded (**rsu** only)
*   The unload destination(s) in S3
*   Any actions to be performed when the unload is complete or has failed.

Each control file is dedicated to a single Redshift cluster. **Rsu** can process
an arbitrary number of control files. **Rsud** must have a single control file.

Control files can be read from the local file system or, if prefixed with
`s3://`, from AWS S3.

They contain the following keys. Keys marked with a leading ... are children of
the previous level 1 key. Any key not listed is ignored. Some keys are
supported only by **rsu** and some only by **rsud** but otherwise the control
file format is the same for both. An _R_ in the table below indicates the key is
required, _O_ indicates optional, - indicates the key is ignored.

| Key | rsu | rsud | Description |
|-----|:---:|:----:|-------------|
| cluster | R | R | A label for the Redshift cluster. This is used to lookup connectivity information for the cluster from the [DynamoDB cluster table](#markdown-header-the-cluster-table). |
| s3access | R | R | A dictionary containing information required by Redshift to write to S3.|
| ...iam\_role | (1)| (1) | The ARN of an IAM role that will be used in the UNLOAD [authorisation parameters](http://docs.aws.amazon.com/redshift/latest/dg/copy-parameters-authorization.html). |
| ...access\_key\_id | (1) | (1) | The IAM access key ID to be used in the UNLOAD [authorisation parameters](http://docs.aws.amazon.com/redshift/latest/dg/copy-parameters-authorization.html). It can be either a literal string or a string starting with `/` indicating the name of an SSM parameter containing the actual value. |
| ...access\_secret\_key | (1) | (1) | The IAM access secret key to be used in the UNLOAD [authorisation parameters](http://docs.aws.amazon.com/redshift/latest/dg/copy-parameters-authorization.html). It can be either a Base64 encoded KMS encrypted string or a string starting with `/` indicating the name of an SSM parameter containing the actual value. |
|defaults|O| R |A dictionary of default values for unload specifications. It can accept the same keys as an [unload specification](#markdown-header-unload-specifications) and the values are automatically included in every unload specification unless overridden in that specification. For **rsud**, this key is almost always required as it specifies the details needed to complete each unload request received via SQS.|
|unload| R | - |A list of [unload specifications](#markdown-header-unload-specifications). **Rsud** gets these from SQS, not the control file.|
|uspec| - | O |A list of keys that are permitted in the unload specifications received via SQS. If not specified, any of the standard keys allowed in an [unload specification](#markdown-header-unload-specifications) are permitted. If specified, only keys in this list are permitted in the SQS request. Values for other keys will be provided by the `defaults` key in the control file. This provides a mechanism for the control file to lock down the value of some keys an unload specification.| 

Notes:

1.  The `s3access` key must contain either an `iam_role` key or
    `access-key-id` and `access-secret-key` keys.

## Unload Specifications

For **rsu**, unload specifications are contained as a list under the `unload`
key in a control file. These are supplemented with the values specified in the
`defaults` key from the control file.

For **rsud**, unload specifications are received in SQS messages. These are
supplemented with the values specified in the `defaults` key from the control
file.

Each unload specification is a dictionary containing the following keys. The
`defaults` key in the control file accepts the same keys. With the exception
of the `vars` key, any value specified in an unload specification will replace
the value from the `defaults` key.

| Key | Required | Description |
| --- | -------- | ----------- |
|dateformat|No|A Redshift [datetime format string](https://docs.aws.amazon.com/redshift/latest/dg/r_FORMAT_strings.html) that will be applied to DATE fields when unloading. The safest value is `YYYY-MM-DD`. [More information on date formats](#markdown-header-date-formatting).|
|exclude|No|Either a relation (table or view) selector or a list of selectors. Any database object that matches any of the selectors will not be unloaded. Exclude selectors have the same syntax and semantics as relation selectors. See note 1 below.|
|on-fail|No|A list of [post unload actions](#markdown-header-post-unload-actions) to perform if the load fails.|
|on-success|No|A list of [post unload actions](#markdown-header-post-unload-actions) to perform if the load succeeds.|
|params|Yes|A list of parameters for the [UNLOAD](http://docs.aws.amazon.com/redshift/latest/dg/r_UNLOAD.html) command. All of the Redshift supported parameters except for the authorisation parameters are supported.|
|relations|(1)|Either a relation (table or view) selector or a list of selectors. See note 2.|
|s3bucket|Yes|The destination S3 bucket for the unloaded data.|
|s3key|Yes|The destination S3 object key for the unloaded data. This value is subject to [parameter expansion](#markdown-header-parameter-expansion) allowing a single value to apply to multiple unloaded objects without conflict.|
|schema|Yes|The name of the schema containing relations to unload.|
|tables|(1)|Either a table selector or a list of selectors. See note 2.|
|vars|No|A dictionary of arbitrary values. These are not used directly but makes them available for [parameter expansion](#markdown-header-parameter-expansion). Unlike all other unload specification keys, the `vars` in any unload specification are merged with any `vars` in the `defaults` rather than replacing it.|
|views|(1)|Either a view selector or a list of selectors. See note 2.|
|where|No|An optional WHERE predicate (without the `WHERE` keyword) for the SELECT query used to unload the data. This value is subject to [parameter expansion](#markdown-header-parameter-expansion).|

Notes:

1.  Each unload specification must have one of the `tables`, `views` or
    `relations` keys. If it has more than one of these keys then only the
    first of `tables`, `views` or `relations`, in that order, is used.

2.  The value for the `tables`, `views` and `relations` keys is either a
    *selector* string or a list of selector strings. A selector can be a
    literal database object name or a string starting with `/`, indicating a
    regex that will be used to match and select database objects in the schema.
    The leading `/` is excluded from the regex. The regex may contain one
    [capture group](#markdown-header-capture-groups-in-relation-selectors) to
    extract a portion as the object name. If a selector does not correspond
    with a database object it is silently discarded. All matching is case
    insensitive.


**Rsu** makes reasonable attempts to avoid unloading the same database object
more than once in a run. **Rsud** only attempts to avoid unloading the same
object more than once if they are both referenced in a single SQS message.

### Capture Groups in Relation Selectors

While the *capture group* facility in a relation selector seems a bit baroque,
it supports some unusual use cases. For example, it allows a simple mechanism
to include or exclude data sets that are loaded using the a/b `switch` mode
loading supported by
[rsDropLoader2](https://bitbucket.org/murrayandrews/evw/src/master/handlers/rsDropLoader2.md).

Switch mode loading for a data set `xyz` alternately loads data into tables
`xyz_a` and `xyz_b`. At any one time, one of these tables has data and the other
is empty. Users access the data via a view `xyz` which is the union of `xyz_a`
and `xyz_b`. The unload operation must also work via the view.

#### Example 1

The following unload specification will include all views in a schema that are
sitting above a/b switch tables:

```yaml
unload:
    schema: myschema
    tables: "/(.*)_a"  # Capture the bit before the _a
```

The `tables` key selects all tables in the schema that end in `_a` and extracts
the prefix as the database object to be unloaded, even though that is in fact a
view. The selector applies at the object discovery phase, not at the unloading
phase.

#### Example 2

The following unload specification will select all tables in a schema that are
not loaded using a/b table switching:

```yaml
unload:
    schema: myschema
    tables: "/."
    exclude: "/_[ab]$"
```


### Sample Control Files

#### Example 1

This example is contrived to show a lot of the basic features of **rsu** but
does not use any special YAML tricks.


```yaml
# ------------------------------------------------------------------------------
# This gets looked up in the DynamoDB cluster table
# ------------------------------------------------------------------------------

cluster: my-cluster

# ------------------------------------------------------------------------------
# Must be either iam_role (ARN) or access-key-id/access-secret-key combo
# ------------------------------------------------------------------------------

s3access:
  access_key_id: AKIAIV449FHFHF48HHEQ
  access_secret_key: /my_ssm_parameter_with_secret_key

# ------------------------------------------------------------------------------
# Defaults applied to every unload specification unless overridden
# ------------------------------------------------------------------------------

defaults:
  vars:
    v1: abc
    v2: xyz
  s3bucket: "my-s3-bucket"
  s3key: "unload/{{schema}}/{{relation}}/{{ts.strftime('%Y%m%d')}}/"
  schema: public
  params:
    - manifest
    - bzip2
    - allowoverwrite
    - parallel on
    - delimiter '|'
  # Databases objects ending in _bak, _a or _b won't be unloaded.
  exclude:
    - "/_[ab]$"
    - "/_bak$"

  # . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
  # This is a list of actions to perform if the job succeeds. Actions have
  # parameters specific to the action. Some parameters may support Jinja2
  # rendering of the value -- hence the {{...}} parameter injection syntax.

  on-success:
    # Send a JSON formatted SQS message in Matillion friendly format.
    - action: sqs
      queue: notify
      message:
        group: "Matillion Project Group"
        project: "Matillion Project"
        version: "Matillion Version"
        environment: "Matillion Environment Name"
        job: "Orchestration Job Name"
        variables:
          relation: "{{r}}"
          schema: "{{s}}"

    # Send an email
    - action: ses
      to:
        - fred.bloggs@gmail.com
        - harry.houdini@somewhere.com
      subject: "Unload OK {{s}}.{{r}}"
      message: "It worked!: {{info}}"

  # . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
  # This is a list of actions to perform if the job fails.
  on-fail:
    - action: sns
      topic: "arn:aws:sns:ap-southeast-2:589458948954:events"
      subject: "Oh no - unload broke: {{s}}.{{r}}"
      message: |-
        Unload of relation {{s}}.{{r}} failed
        The problem was {{i}}.

# ------------------------------------------------------------------------------
# These are our unload specifications
# ------------------------------------------------------------------------------

unload:
  # . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
  # Unload all views in myschema. Use custom UNLOAD parameters.
  - schema: myschema
    views: "/."     # Regex matches anything
    params:
      - manifest
      - bzip2
      - allowoverwrite
      - parallel on
      - delimiter '|'
      - encrypted
      - kms_key_id 'alias/my-kms-key'
  # . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
  # Unload a single table from public. The schema name comes from the defaults.
  - tables: mytable
  # . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
  # Unload all tables from public. The schema name comes from the defaults.
  - tables: "/."    # Regex matches anything
  # . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
  # Unload a named relation (table or view)
  - schema: myotherschema
    relations:
      - my_table_or_view
      - my_table_or_view     # Duplicate will be ignored

```

#### Example 2

This example is a bit more realistic. It also shows how to use some of the
less common features of YAML (anchors and references) to make the control
better factored and more concise.

```yaml
# ------------------------------------------------------------------------------
# Locals -- These are standard YAML anchors for reuse in unload specs. They are
#           pre-processed by the YAML loader and are ignored by rsu.
# ------------------------------------------------------------------------------

locals:

  # . . . . . . . . . . . . . . . . . . . . 
  # CloudWatch metric dimensions
  dimensions: &dimensions   # Convention is CamelCase for dimension names
    Cluster: "{{cluster}}"
    Database: "{{database}}"
    Schema: "{{schema}}"
    Relation: "{{relation}}"
  # . . . . . . . . . . . . . . . . . . . . 
  # CloudWatch metric definition for failed unloads
  cwmfail: &cwmfail
    action: cwm
    namespace: "{{v.namespace}}"
    timestamp: ustart  # Name of the key -- no rendering
    dimensions: *dimensions
    metrics:
      - name: "UnloadFailed"
        value: 1
  # . . . . . . . . . . . . . . . . . . . . 
  # CloudWatch metric definition for successful unloads
  cwmsuccess: &cwmsuccess
    action: cwm
    namespace: "{{v.namespace}}"
    timestamp: ustart  # Name of the key -- no rendering
    dimensions: *dimensions
    metrics:
      - name: "RunTime"
        unit: "Seconds"
        value: "{{eseconds|round(1)}}"
      - name: "UnloadFailed"
        value: 0
  # . . . . . . . . . . . . . . . . . . . . 
  # Standard Matillion SQS action to launch a job. Requires some vars
  # to be defined but the object structure is correct. The message will be
  # rendered as JSON
  matillion: &matillion
    action: sqs
    queue: "{{v.mt_queue}}"
    message:
      group: "{{v.mt_group}}"
      project: "{{v.mt_project}}"
      version: "{{v.mt_version}}"
      environment: "{{v.mt_environment}}"
      job: "{{v.mt_job}}"
      variables:
        schema: "{{schema}}"
        relation: "{{relation}}"
    

# ------------------------------------------------------------------------------
# This gets looked up in the DynamoDB cluster table
# ------------------------------------------------------------------------------

cluster: my-cluster

# ------------------------------------------------------------------------------
# Must be either iam_role (ARN) or access-key-id/access-secret-key combo
# ------------------------------------------------------------------------------

s3access:
  iam_role: "arn:aws:iam::589458948954:role/redshift"

# ------------------------------------------------------------------------------
# Defaults applied to every unload specification unless overridden
# ------------------------------------------------------------------------------

defaults:
  vars:
    namespace: "Red Route 1"  # CloudWatch metric namespace
    mt_group: "Matillion Project Group"
    mt_project: "Matillion Project"
    mt_version: "default"
  s3bucket: "my-s3-bucket"
  s3key: "unload/{{schema}}/{{relation}}/{{ts.strftime('%Y%m%d')}}/"
  schema: myschema
  params:
    - manifest
    - bzip2
    - allowoverwrite
    - parallel on
    - delimiter '|'
  # Databases objects ending in _bak won't be unloaded.
  exclude:
    - "/_bak$"

  # . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
  on-success:
    # Post some Cloudwatch metrics
    - *cwmsuccess

  # . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
  on-fail:
    - action: sns
      topic: "arn:aws:sns:ap-southeast-2:589458948954:events"
      subject: "Oh no - unload broke: {{s}}.{{r}}"
      message: |-
        Unload of relation {{s}}.{{r}} failed
        The problem was {{i}}.
    # Post some Cloudwatch metrics
    - *cwmfail

# ------------------------------------------------------------------------------
# These are our unload specifications
# ------------------------------------------------------------------------------

unload:
  # . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
  # Unload an important table then kick off a Matillion job
  - tables: very_important_table
    vars:
      mt_environment: "Matillion environment name"
      mt_job: "Matillion job name"
    on-success:
      # Send a JSON formatted SQS message in Matillion friendly format.
      - *matillion
      - *cwmsuccess

  # . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
  # Unload an unimportant table -- no post unload actions.
  - tables: not_important_table
    on-success: []  # Override defaults with empty action list
    on-fail: []

  # . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
  # Unload tables that are supplied to the cluster by a/b switch loading
  # We scan for tables but the captured name will reference the union view.
  - tables: "/(.*)_a$"  # Capture the bit before the _a

  # . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
  # Unload tables that are not supplied to the cluster by a/b switch loading
  - tables: "/."
    exclude: "/_[ab]$"
```

## Post Unload Actions

[Unload specifications](#markdown-header-unload-specifications) may contain the
`on-fail` and/or `on-success` keys that specify a list of *actions* to be
performed when the unload fails or succeeds respectively. These actions are only
executed if the unload is attempted. If an error occurs prior to the unload
being attempted, neither the success nor the fail actions will be performed.

Each *action* is a dictionary consisting of:

1.  [Keys common to all actions](#markdown-header-keys-common-to-all-actions)
2.  [Keys specific to the given action type](#markdown-header-available-actions).

### Keys Common to All Actions

|Key|Required|Description|
|-|-|-|
|action|Yes|Specifies the type of action to perform. Allowed values are specified [below](#markdown-header-available-actions).|


### Available Actions

This section lists the currently supported action types and the keys specific to
each.

A number of action parameters support rendering of their contents using 
[Jinja2](http://jinja.pocoo.org). See [Parameter Expansion](#markdown-header-parameter-expansion)
for a list of the data items that are made available to the Jinja2 renderer.


Action handler parameters that support rendering with Jinja2 are indicated below.

#### cwm

The `cwm` action creates custom CloudWatch metrics.

|Key|Required|Jinja|Description|
|-|-|-|-|
|namespace|Yes|Yes|The CloudWatch metric namespace.|
|timestamp|No|No|The *name* of the parameter from the [parameter expansion](#markdown-header-parameter-expansion) list that provides the UTC timestamp for the event. If specified, it must be either `ustart` or `uend` depending on whether you want metric timestamps to align with the start or end of the unload. The former is recommended. If not provided, the current UTC time is used.|
|dimensions|Yes|Yes|A dictionary of CloudWatch dimensions with each value being Jinja2 rendered.|
|metrics|Yes|-|A list of metrics. Each metric is a dictionary containing the following three keys.|
|...name|Yes|No|The metric name. CloudWatch convention is to use CamelCase.|
|...unit|No|No|One of the standard CloudWatch metric units. If not specified, `None` is used.|
|...value|Yes|Yes|The metric value. The rendered result must be convertible to a float.|


Sample:

```yaml
# Report a run-time and a success related metric for unloads
on-success:
  - action: cwm
    namespace: "Redshift Unloads"
    timestamp: ustart  # Name of the key -- no rendering
    dimensions: # Convention is CamelCase for dimension names
      Cluster: "{{cluster}}"
      Database: "{{database}}"
      Schema: "{{schema}}"
      Relation: "{{relation}}"
    metrics:
      - name: "RunTime"
        unit: "Seconds"
        value: "{{eseconds|round(1)}}"
      - name: "UnloadFailed"
        value: 0

# We don't report run time for failed loads -- messes with the stats.
on-fail:
  - action: cwm
    namespace: "Redshift Unloads"
    timestamp: ustart  # Name of the key -- no rendering
    dimensions: # Convention is CamelCase for dimension names
      Cluster: "{{cluster}}"
      Database: "{{database}}"
      Schema: "{{schema}}"
      Relation: "{{relation}}"
    metrics:
      - name: "UnloadFailed"
        value: 1
```
#### ses

The `ses` action sends an email using AWS SES.

|Key|Required|Jinja|Description|
|-|-|-|-|
|message|Yes|Yes|Message body.|
|subject|Yes|Yes|Message subject.|
|to|Yes|No|Either a single email address or a list of email addresses.|


Sample:

```yaml
# Note the |- in the message key controls whitespace - standard YAML
# See http://yaml-multiline.info for other options.
on-fail:
  - action: ses
    to:
      - fred.bloggs@gmail.com
      - harry.houdini@somewhere.com
    subject: "Unload failed {{s}}.{{r}}"
    message: |-
      Unload of relation {{s}}{{r}} failed
      The problem was {{info}}.
```

#### sns

The `sns` action sends a message to an SNS topic. Messages can be either normal
strings or fully-formed JSON encoded objects.

|Key|Required|Jinja|Description|
|-|-|-|-|
|message|Yes|Yes|Either a string or an object that will be JSON encoded and sent as the SNS message.|
|subject|No|Yes|Message subject.|
|topic|Yes|No|The topic ARN.|

Sample:

```yaml
- action: sns
  topic: "arn:aws:sns:ap-southeast-2:589458948954:events"
  subject: "Unload failed {{s}}.{{r}}"
  message: |-
    Unload of relation {{s}}.{{r}} failed
    The problem was {{i}}.
```

#### sqs

The `sqs` action sends a message to an SQS queue. Messages can be either normal
strings or fully-formed JSON encoded objects.

|Key|Required|Jinja|Description|
|-|-|-|-|
|message|Yes|Yes|Either a string or an object that will be JSON encoded and sent as the SQS message.|
|queue|Yes|No|The queue name or URL.|

Sample:

In this example, as the `message` is itself an object, it will be JSON encoded
before being sent.

```yaml
- action: sqs
  queue: notify
  message:
    group: "Matillion Project Group"
    project: "Matillion Project"
    version: "Matillion Version"
    environment: "Matillion Environment Name"
    job: "Orchestration Job Name"
    variables:
      relation: "{{r}}"
      schema: "{{s}}"
```
## Parameter Expansion

The `s3key` and `where` keys in [unload specifications](#markdown-header-unload-specifications)
and a number of the parameters for [post unload
actions](#markdown-header-post-unload-actions) are rendered using
[Jinja2](http://jinja.pocoo.org) to allow injection of parameters relevant to
the individual unload operation.

All Jinja2 constructs in control files must be quoted otherwise the YAML decoder
will be confused by the Jinja2 syntax.

All of the injected parameters are effectively Python objects so the normal
Jinja2 syntax and Python methods for those objects can be used in the Jinja2
templates. This is particularly useful for the
[datetime](https://docs.python.org/3/library/datetime.html#datetime-objects)
objects as `strftime()` becomes available. For example, the S3 location of
the unload (`s3key`) can be dynamically set to include components such as the
schema, table name, unload date etc.

The following variables are made available to the renderer. Each variable has
a full name and a shortcut name. Either can be used to reference variables in
Jinja2 rendered strings. The items marked with * are only available to [post
unload actions](#markdown-header-post-unload-actions).

|Name|Shortcut|Type|Description|
|-|-|-|-|
|cluster|c|string|Cluster label. This is value used to lookup the [DynamoDB cluster table](#markdown-header-the-cluster-table).|
|database|db|string|The database in the Redshift cluster. This is obtained from the [DynamoDB cluster table](#markdown-header-the-cluster-table).|
|end*|te|[datetime](https://docs.python.org/3/library/datetime.html#datetime-objects)|The local time at which the unload ended (successfully or otherwise).|
|eseconds*|es|integer|The elapsed seconds for the unload (successfully or otherwise).|
|etime*|et|[timedelta](https://docs.python.org/3/library/datetime.html#timedelta-objects)|The elapsed time for the unload (successfully or otherwise).|
|info*|i|string|A informational message about the outcome of the unload.|
|relation|r|string|Name of the unloaded relation (table or view) without any schema component.|
|s3bucket|b|string|The destination S3 bucket for the unload.|
|s3key*|k|string|The destination S3 object key for the unload.|
|schema|s|string|Name of the schema containing the unloaded relation.|
|start|ts|[datetime](https://docs.python.org/3/library/datetime.html#datetime-objects)|The local time at which the unload started.|
|uend*|ue|[datetime](https://docs.python.org/3/library/datetime.html#datetime-objects)|The UTC time at which the unload ended (successfully or otherwise).|
|ustart|us|[datetime](https://docs.python.org/3/library/datetime.html#datetime-objects)|The UTC time at which the unload started.|
|vars|v|dict|The `vars` dictionary from the unload spec. The var `xyz` could be referenced as either `{{v.xyz}}`, `{{vars.xyz}}`, `{{v['xyz']}}` or `{{vars['xyz']}}`.|


## Date Formatting

Redshift has some very nasty behaviour in its date handling. It will UNLOAD
date fields with years less than 100 in `YY-MM-DD` format. Madness.

Redshift COPY using `DATEFORMAT 'auto'` assumes dates in `YY-MM-DD` format must
be Y2K adjusted (i.e. they are moved to post 2000). More madness.

So an UNLOAD followed by a COPY will mangle dates. While the COPY command could
specify `DATEFORMAT 'YY-MM-DD'`, this will then fail for any dates with years
greater than 99. So if the column contains dates before and after the year 100,
you're in trouble.

In short, there is no reliable way to UNLOAD and then COPY a data set containing
dates without manually formatting date fields.

To get around this singular piece of Redshift genius, use the `dateformat` key
in unload specifications.

If the `dateformat` key is present, and the relation has one or more DATE
fields, **rsu**/**rsud** will construct a SELECT statement for the unload that
includes all of the columns, applying the given date format to DATE columns.
This is likely to impact UNLOAD performance, so its best to avoid using it if
you are certain all dates have years greater than 99. The safest format value is
probably `YYYY-MM-DD`.

If the `dateformat` key is not present, or the relation has no DATE columns,
**rsu**/**rsud** will simply use `SELECT *` for the target relation in the
UNLOAD command.

## The Cluster Table

The _cluster table_ is a DynamoDB table that contains connection information for
one or more target Redshift clusters. **Rsu**/**rsud** can use the same cluster
table as that used by
[rsDropLoader2](https://bitbucket.org/murrayandrews/evw/src/master/handlers/rsDropLoader2.md#markdown-header-the-cluster-table).

The default table name is `clusters` but can be overridden
with the `RSUNLOAD_CLUSTER_TABLE` environment variable or the `--cluster-table`
command line option.

Unlike **rsDropLoader2**, **rsu**/**rsud** requires the `user` and `password`
fields to be present in the cluster table for Redshift clusters to which it
connects.  **RsDropLoader2** will ignore the `user` and `password` fields for
Redshift clusters.  Otherwise, the configuration is identical for both.


## S3 Bucket Security Checks

**Rsu**/**rsud** performs some basic checks on the security of target S3 buckets
to reduce the risk of unloading data to somewhere unsafe. This is a convenience
only and should not be relied upon for securing your data.

By default, if any of the following are true, the bucket will not be used
for unloaded data:

*   The bucket has any form of public access
*   The bucket does not have default encryption enabled
*   Server logging is not enabled on the bucket
*   The bucket is owned by an AWS account other than the one associated with the
    profile being used by **rsu**/**rsud**.

These security checks can be disabled using the `--insecure` command line option.


## Access Requirements

### IAM

**Rsu**/**rsud** itself has the following IAM requirements:

*   S3:
    *   Get objects (if control files are located in S3).
    *   List all buckets, get bucket logging configuration, get bucket
        encryption and get bucket ACLs (unless
        [S3 bucket security checks](#markdown-header-s3-bucket-security-checks)
        are disabled).
*   KMS decrypt (e.g credentials that may be KMS encrypted)
*   SSM to access any authentication parameters stored therein.
*   DynamoDB to read the cluster table.
*   SNS, SES, SQS and CloudWatch as required by the
    [post unload actions](#markdown-header-post-unload-actions).

**Rsud** has the following additional requirements:

*   SQS for the unload request queue
    *   ListQueues
    *   GetQueueUrl
    *   ReceiveMessage
    *   DeleteMessage.
    
### Redshift

**Rsu**/**rsud** requires `USAGE` rights on all schemas that it must scan and
`SELECT` access on all relations that it will unload.

The S3 authorisation parameters provided for use with UNLOAD commands
must allow:

*   Writing to the target S3 buckets
*   Use of any KMS key specified in the `kms_key_id` UNLOAD parameter
    or otherwise the default KMS key for the bucket.

## Author

Murray Andrews

## Licence

[BSD 3-clause licence](http://opensource.org/licenses/BSD-3-Clause).

## See Also

See the [rsu man page](rsu.md) and the [rsud man page](rsud.md).
