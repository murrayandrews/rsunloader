#!/usr/bin/env python3

"""
Automated RedShift Extractor With Inline Parameter Expansion

... aka Redshift Unloader (rsu).

The is a tool for bulk unloading data from Redshift clusters to S3. When coupled
with rsDropLoader2, it allows bulk transfer of data between clusters.

................................................................................
Copyright (c) 2017, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

# ..............................................................................
# region imports

from __future__ import print_function

import argparse
import logging
import os
import re
import sys
import time
from datetime import datetime
from ssl import SSLContext
from threading import Thread

import jinja2
import pg8000
import yaml

# noinspection PyUnresolvedReferences
import common.actions  # contents are dynamically invoked.
from common.aws_utils import kms_decrypt, \
    redshift_relations_in_schema, redshift_get_column_info, \
    get_ssm_param, s3_load_yaml, s3_split, s3_check_bucket_security
from common.date import local_to_utc, round_timedelta
from common.decorators import static_vars
from common.logging import setup_logging
from common.utils import dict_check, mget, lock_file

try:
    # Python 2
    # noinspection PyUnresolvedReferences, PyCompatibility
    from Queue import Queue
except ImportError:
    # Python 3
    # noinspection PyUnresolvedReferences, PyCompatibility
    from queue import Queue

import boto3

# endregion imports
# ..............................................................................

# ..............................................................................
# region constants

__author__ = 'Murray Andrews'
__version__ = '1.8.0'

pg8000.paramstyle = 'format'
assert pg8000.threadsafety >= 1, "Redshift driver is not threadsafe - bye"

PROG = os.path.splitext(os.path.basename(sys.argv[0]))[0]

LOGNAME = PROG  # Set to None to include boto which uses root logger.
LOG = logging.getLogger(name=LOGNAME)
LOGLEVEL = 'info'  # Default logging level

THREADS = 2

# These constants control reuse of connections to Redshift. Even with reuse
# enabled, connections are not shared across threads.
MAIN_CONNECTIONS_AGE = 10 * 60  # Main thread can hold connection for 10 mins
REUSE_UNLOAD_CONNECTIONS = False

CONTROL_REQUIRED_KEYS = {'cluster', 'unload', 's3access'}
USPEC_REQUIRED_KEYS = {'s3bucket', 's3key', 'schema', 'params'}
USPEC_SELECTOR_KEYS = {'tables', 'views', 'relations'}
USPEC_OPTIONAL_KEYS = USPEC_SELECTOR_KEYS | {
    'dateformat', 'exclude', 'vars',
    'on-success', 'on-fail', 'where'
}

S3_SECURITY_CHECK_AGE = 60 * 60  # Check bucket security once per hour
S3ACCESS_OPTIONAL_KEYS = {'access_key_id', 'access_secret_key', 'iam_role'}
CLUSTER_REQUIRED_KEYS = {'cluster', 'host', 'port', 'database'}
REDSHIFT_UNLOAD_KEYS = {
    'ADDQUOTES', 'ALLOWOVERWRITE', 'BZIP2', 'DELIMITER', 'ENCRYPTED', 'ESCAPE',
    'FIXEDWIDTH', 'GZIP', 'KMS_KEY_ID', 'MANIFEST', 'MAXFILESIZE' 'NULL',
    'PARALLEL',
}

CLUSTER_TABLE = os.environ.get('RSUNLOAD_CLUSTER_TABLE', 'clusters')
# Cluster status codes -- used for the data cluster.
CLUSTER_ACTIVE = 'on'  # Load requests can be processed

# Regular expressions for various things
RE = {
    'schema': re.compile(r'^\w+$'),  # Schema names
    'relation': re.compile(r'^[\w#$]+$'),  # Allow # and $ characters in the relation name
    # Credit for the following:
    #   http://info.easydynamics.com/blog/aws-s3-bucket-name-validation-regex
    's3bucket': re.compile(
        r'^([a-z]|(\d(?!\d{0,2}\.\d{1,3}\.\d{1,3}\.\d{1,3})))([a-z\d]|(\.(?![.-]))|(-(?!\.))){1,61}[a-z\d]$'
    )
}


# endregion constants
# ..............................................................................


# ------------------------------------------------------------------------------
def process_cli_args():
    """
    Process the command line arguments.
    :return:    The args namespace.
    """

    argp = argparse.ArgumentParser(
        prog=PROG,
        description='Redshift table unloader'
    )

    argp.add_argument('-c', '--no-colour', '--no-color', dest='colour', action='store_false',
                      default=True, help='Don\'t use colour in information messages.')

    argp.add_argument('--cluster-table', dest='cluster_table', action='store',
                      default=CLUSTER_TABLE,
                      help='Name of the DynamoDB table containing connection'
                           ' information for Redshift clusters. Default is'
                           ' "{}".'.format(CLUSTER_TABLE))

    argp.add_argument('--dry-run', dest='dry_run', action='store_true',
                      help='Do a dry run. No unloads are performed but relations'
                           ' that would be unloaded are identified.')

    argp.add_argument('--insecure', action='store_true',
                      help='Allow writing to insecure buckets. By default, some'
                           ' *basic* security checks are performed on the bucket'
                           ' and the unload process to prevent accidents.')

    argp.add_argument('-l', '--level', metavar='LEVEL', default=LOGLEVEL,
                      help='Print messages of a given severity level or above.'
                           ' The standard logging level names are available but info,'
                           ' warning and error are most useful. Default is {}.'.format(LOGLEVEL))

    argp.add_argument('--lock-file', dest='lock_file', action='store',
                      help='Create a lock file with the given name to prevent'
                           ' two instances using the same lock file from running'
                           ' simultaneously.')
    argp.add_argument('--log', action='store',
                      help='Log to the specified target. This can be either a file'
                           ' name or a syslog facility with an @ prefix (e.g. @local0).')

    argp.add_argument('--profile', action='store', help='As for AWS CLI.')

    argp.add_argument('-t', '--threads', action='store', type=int, default=THREADS,
                      help='Run this many threads. Default {}.'.format(THREADS))

    argp.add_argument('--tag', action='store', default=PROG,
                      help='Tag log entries with the specified value. The default is {}.'.format(PROG))

    argp.add_argument('-v', '--version', action='version', version=__version__,
                      help='Print version and exit.')

    argp.add_argument('control_files', nargs='+', metavar='control-file',
                      help='Control files (prefix with s3:// for S3 based files.')

    return argp.parse_args()


# ------------------------------------------------------------------------------
class ControlFile(object):
    """
    Interface to the control file structure
    """

    # --------------------------------------------------------------------------
    def __init__(self, control_file, aws_session=None):
        """
        Read the specified control file (which may be in S3) and extract unload
        specs one by one. This is a generator.

        :param control_file:    Name of the control file. If it starts with s3://
                                it will be downloaded from S3.
        :param aws_session:     A boto3 Session object. If not supplied a
                                default session will be used.
        :type control_file:     str
        :type aws_session:      boto3.Session

        """

        if control_file.startswith('s3://'):
            bucket, key = s3_split(control_file)
            if not bucket or not key:
                raise Exception('{}: bad control file name'.format(control_file))
            self._data = s3_load_yaml(bucket, key, aws_session=aws_session)
            LOG.debug('Read %s from S3', control_file)
        else:
            with open(control_file, 'r') as fp:
                self._data = yaml.safe_load(fp)
            LOG.debug('Read %s locally', control_file)

        try:
            dict_check(self._data, required=CONTROL_REQUIRED_KEYS)
        except Exception as e:
            raise Exception('{}: {}'.format(control_file, e))

        self.cluster = self._data['cluster']
        self._s3cred = None
        self.control_file = control_file
        self.aws_session = aws_session if aws_session else boto3.Session()

    # --------------------------------------------------------------------------
    def __getitem__(self, item):
        """
        Provide dictionary-like access to control info.

        :param item:
        :return:
        """

        return self._data[item]

    # --------------------------------------------------------------------------
    @property
    def s3cred(self):
        """
        Extract the S3 credentials in a form useable in an UNLOAD command.

        :return:        An S3 credentials string for use in Redshift UNLOAD.
        :rtype:         str
        """

        if not self._s3cred:
            s3c = self._data['s3access']
            try:
                dict_check(s3c, optional=S3ACCESS_OPTIONAL_KEYS)
            except ValueError as e:
                raise Exception('{}: s3access: {}'.format(self.control_file, e))

            if 'access_key_id' in s3c and 'iam_role' in s3c:
                raise Exception(
                    '{}: s3access: Must contain role_arn or access_key_id/access_secret_key'.format(
                        self.control_file
                    ))

            # ----------------------------------------
            if 'iam_role' in s3c:
                # Check for a role ARN
                self._s3cred = "IAM_ROLE '{}'".format(s3c['iam_role'])
            else:
                # Access keys instead of role.
                try:
                    dict_check(s3c, required={'access_key_id', 'access_secret_key'}, optional=[])
                except ValueError as e:
                    raise Exception('{}: s3access: {}'.format(self.control_file, e))

                # Get the access key Id -- either in line or from SSM
                try:
                    if s3c['access_key_id'].startswith('/'):
                        access_key_id = get_ssm_param(s3c['access_key_id'])
                    else:
                        access_key_id = s3c['access_key_id']
                except Exception as e:
                    raise Exception('{}: s3access: access_key_id: {}'.format(self.control_file, e))

                # Get the access secret key - either KMS encrypted inline or from SSM
                try:
                    if s3c['access_secret_key'].startswith('/'):
                        access_secret_key = get_ssm_param(s3c['access_secret_key'])
                    else:
                        access_secret_key = kms_decrypt(
                            s3c['access_secret_key'],
                            aws_session=self.aws_session
                        ).decode('utf-8').strip()
                except Exception as e:
                    raise Exception(
                        '{}: s3access: access_secret_key: {}'.format(self.control_file, e))

                self._s3cred = "ACCESS_KEY_ID '{}' SECRET_ACCESS_KEY '{}'".format(
                    access_key_id, access_secret_key)
        return self._s3cred

    # --------------------------------------------------------------------------
    @property
    def uspecs(self):
        """
        Return the unload specs one at a time. This is a generator.

        :return:                One unload spec per call.

        """

        defaults = self._data.get('defaults', {})
        defaults.setdefault('vars', {})
        if not isinstance(defaults['vars'], dict):
            raise Exception('defaults: vars must be a dict')

        n = 0
        for d in self._data.get('unload', []):
            n += 1
            uspec = dict(defaults)
            uspec.update(d)

            # ----------------------------------------
            # Check user defined vars. We treat the vars element differently.
            # Its values are merged into the defaults - not replaced
            v = uspec.get('vars', {})
            if not isinstance(v, dict):
                raise Exception('unload spec {}: vars must be a dict'.format(n))
            uspec['vars'] = dict(defaults['vars'])
            uspec['vars'].update(v)

            # ----------------------------------------
            try:
                dict_check(uspec, required=USPEC_REQUIRED_KEYS, optional=USPEC_OPTIONAL_KEYS)
            except ValueError as e:
                raise Exception('unload spec {}: {}'.format(n, e))

            # ----------------------------------------
            # Check S3 bucket name
            if not RE['s3bucket'].match(uspec['s3bucket']):
                raise Exception('unload spec {}: bad s3 bucket name: {}'.format(
                    n, uspec['s3bucket']))
            # ----------------------------------------
            # Check schema name
            if not RE['schema'].match(uspec['schema']):
                raise Exception('unload spec {}: bad schema name: {}'.format(
                    n, uspec['schema']))

            # ----------------------------------------
            # Make sure exactly one of the relation selectors is present
            if len(set(uspec) & USPEC_SELECTOR_KEYS) != 1:
                raise Exception('unload spec {}: must have exactly one of {}'.format(
                    n, ', '.join(sorted(USPEC_SELECTOR_KEYS))))

            # ----------------------------------------
            # Check UNLOAD params
            uspec.setdefault('params', [])
            if not isinstance(uspec['params'], list):
                raise Exception('unload spec {}: unload params must be a list'.format(n))
                # Get a normalised list of all parameter keywords
            unload_params = {lp.split()[0].upper() for lp in uspec['params']}

            bad_params = unload_params - REDSHIFT_UNLOAD_KEYS
            if bad_params:
                raise Exception('unload spec {}: Invalid params: {}'.format(
                    n, ', '.join(sorted(bad_params))))

            yield uspec

    # --------------------------------------------------------------------------
    @uspecs.setter
    def uspecs(self, value):
        """
        Install some unload specs. This is required by rsud.
        :param value:       A list of unload specs or a single unlod spec which
                            is a dict.
        :type value:        list | dict

        """

        if isinstance(value, dict):
            self._data['unload'] = [value]
        elif not isinstance(value, list):
            raise ValueError(
                'Bad unload specs - must be dict or list of dicts not {}'.format(
                    type(value)))


# ------------------------------------------------------------------------------
class RedshiftCluster(object):
    """
    Manage an interface to a Redshift cluster.
    """

    # --------------------------------------------------------------------------
    def __init__(self, cluster, cluster_info, aws_session=None):
        """

        :param cluster:         Cluster label
        :param cluster_info:    Cluster information
        :type cluster:          str
        :type cluster_info:     dict
        """

        self.name = cluster.lower()
        self._info = cluster_info
        self.aws_session = aws_session
        self._conn = None
        self._conn_time = None

        # ---------------------------------------
        # Check the cluster specs. Ignore the optional keys
        try:
            dict_check(cluster_info, CLUSTER_REQUIRED_KEYS)
        except Exception as e:
            raise Exception('Cluster {}: {}'.format(cluster, e))

        self._info['port'] = int(cluster_info['port'])
        self.port = int(cluster_info['port'])

        if not self.active:
            raise Exception('Cluster is not active')

        # ----------------------------------------
        # Get the user ID
        if self['user'].startswith('/'):
            self._info['user'] = get_ssm_param(self['user'])

        # ----------------------------------------
        # Get the password
        if self['password'].startswith('/'):
            # Get it from SSM
            self._info['password'] = \
                get_ssm_param(self['password'], aws_session=self.aws_session).strip()
        else:
            # Assume its a KMS encrypted string.
            self._info['password'] = \
                kms_decrypt(self['password'], aws_session=self.aws_session).decode('utf-8').strip()

    # --------------------------------------------------------------------------
    def __getitem__(self, item):
        """
        Provide dictionary-like access to cluster info.

        :param item:
        :return:
        """

        return self._info[item]

    # --------------------------------------------------------------------------
    @property
    def active(self):
        """
        :return:    True if the cluster is active.
        :rtype:     bool
        """

        return self._info.get('status', CLUSTER_ACTIVE) == CLUSTER_ACTIVE

    # --------------------------------------------------------------------------
    def connect(self):
        """
        Return a connection to the Redshift cluster. It is up to the caller to
        close the connection.

        Connection details (e.g. user, password) are cached -- they are not
        looked up each time. This saves a lot of API and DynamoDB traffic but it
        means that a password change will only be recognised after a restart.

        :return:                A database connection.
        :rtype:                 redshift.Connection
        """

        try:
            ssl_context = SSLContext() if self['ssl'] else None
        except KeyError:
            ssl_context = None

        if ssl_context:
            ssl_context.load_default_certs()

        connect_params = {
            'host': self['host'],
            'port': self['port'],
            'database': self['database'],
            'user': self['user'],
            'password': self['password'],
            # 'ssl': False
            'ssl_context': ssl_context
        }

        # ----------------------------------------
        # Connect

        LOG.debug('Cluster %s: connecting', self.name)
        try:
            conn = pg8000.connect(**connect_params)
        except pg8000.InterfaceError as e:
            raise Exception('Cannot connect to cluster {}: {} - {}'.format(
                self.name, e.args[0], e.args[1]))
        LOG.debug('Cluster %s: connected', self.name)

        return conn

    # --------------------------------------------------------------------------
    def relations_in_schema(self, schema, include=None, exclude=None,
                            tables=True, views=True, conn_age=0):
        """
        Return a list of relations (tables and/or views) in the specifed schema
        in the cluster. This is only called in the main thread so safe to reuse
        connections.

        :param schema:      Schema name.
        :param include:     A relation specifier or list of relation specifiers.
                            A relation specifier is either a literal relation
                            name or a regex starting with / that is used to
                            select relations from the schema. The leading  / is
                            just a flag that its a regex and is excluded from
                            the matching process.  The regex may contain one
                            capture group to extract a portion as the table
                            name.  If a literal does not exist as a relation it
                            is silently discarded. All matching is case
                            insensitive.
        :param exclude:     A relation specifier or list of relation specifiers.
                            Any relation matching one of the specifiers is
                            excluded from the returned list.  Excludes override
                            includes.
        :param tables:      If True include tables. Default True.
        :param views:       If True include views. Default True.
        :param conn_age:    A connection to the cluster for this purpose will
                            be reused if it is less than this many seconds old.
                            Default 0.
        :type schema:       str
        :type include:      str | list
        :type exclude:      str | list
        :type tables:       bool
        :type views:        bool
        :type conn_age:     int
        :return:            A set of relation names.
        :rtype:             set[str]
        """

        if isinstance(include, str):
            include = [include]
        if isinstance(exclude, str):
            exclude = [exclude]
        if not exclude:
            exclude = []

        # Precompile all the regexes
        include = [re.compile(x[1:], re.IGNORECASE) if x.startswith('/') else x.lower() for x in include if x]
        exclude = [re.compile(x[1:], re.IGNORECASE) if x.startswith('/') else x.lower() for x in exclude if x]

        if not self._conn:
            self._conn = self.connect()
            self._conn_time = datetime.now()
        elif (datetime.now() - self._conn_time).total_seconds() >= conn_age:
            # This connection is too old. This is a guard against long held connections failing.
            self._conn.close()
            LOG.debug('Cluster %s: disconnecting old connection', self.name)
            self._conn = self.connect()
            self._conn_time = datetime.now()

        with self._conn.cursor() as cursor:
            relations = redshift_relations_in_schema(cursor, schema, tables=tables, views=views)

        # Find a set of candidate relations to include
        candidate_relations = set()
        for r in relations:
            for inc in include:
                if isinstance(inc, str):
                    # Literal match
                    if inc == r:
                        candidate_relations.add(r)
                else:
                    # Regex time
                    m = inc.search(r)
                    if m:
                        # Pick out the capture group if present else whole string matched
                        candidate_relations.add(m.group(1) if m.groups() else r)

        # Now process the candidate relations through the exclude list
        exclusions = set()
        for r in candidate_relations:
            LOG.debug('%s: %s.%s is a candidate relation', self.name, schema, r)
            if not RE['relation'].match(r):
                LOG.warning('Discarding relation %s: invalid name', r)
                continue

            for exc in exclude:
                if isinstance(exc, str):
                    # Literal match
                    if exc == r:
                        exclusions.add(r)
                        LOG.debug('%s: %s.%s is being excluded', self.name, schema, r)
                else:
                    # Regex time
                    if exc.search(r):
                        exclusions.add(r)
                        LOG.debug('%s: %s.%s is being excluded', self.name, schema, r)

        return candidate_relations - exclusions


# ------------------------------------------------------------------------------
class ClusterTable(object):
    """
    Manage an interface to the DynameDB cluster table
    """

    # --------------------------------------------------------------------------
    def __init__(self, table_name, aws_session=None):
        """

        :param table_name:      The name of the DynamoDB table containing
                                cluster details.
        :param aws_session:     A boto3 Session. If not supplied a default session
                                will be created.
        :type table_name:       str
        :type aws_session:      boto3.Session
        """

        self._clusters = {}
        self.dyn_cluster_table = table_name
        self.aws_session = aws_session if aws_session else boto3.Session()

    # --------------------------------------------------------------------------
    def __getitem__(self, item):
        """
        Provide dictionary-like access to cluster entries.

        :param item:            Cluster identifier.
        :type item:             str
        :return:                The cluster record.
        :rtype:                 RedshiftCluster
        :raise Exception:       If the cluster spec cannot be retrieved.
        """

        if item not in self._clusters:
            try:
                dyn_table = self.aws_session.resource('dynamodb').Table(self.dyn_cluster_table)
                cluster_info = dyn_table.get_item(Key={'cluster': item})['Item']  # type: dict
            except Exception as e:
                raise Exception('Cluster {}: Cannot get info from DynamoDB table {} - {}'.format(
                    item, self.dyn_cluster_table, e))

            self._clusters[item] = RedshiftCluster(
                cluster=item,
                cluster_info=cluster_info,
                aws_session=self.aws_session
            )

        return self._clusters[item]


# ------------------------------------------------------------------------------
def unload_worker(job_queue, reuse_connections=False, aws_session=None):
    """
    Thread worker to perform UNLOAD operations. It pulls unload jobs from the
    internal queue and proceesses them.

    :param job_queue:       Queue containing jobs. Each job is a dictionary
                            containing:
                            - cluster:  The Cluster object
                            - s3cred:   S3 credentials for authorization param
                                        for UNLOAD
                            - uspec:    The unload spec itself.
                            - relation: The relation to UNLOAD (no schema).
                            - dry_run:  If True don't do any actual unloads.
    :param reuse_connections: If True, reuse cluster connections. Default False.
                            Best to stick with the default.
    :param aws_session:     A boto3 Session object.

    :type job_queue:        Queue
    :type reuse_connections: bool
    :type aws_session:      boto3.Session
    """

    LOG.debug('Starting')

    if not aws_session:
        aws_session = boto3.Session()

    conn = None

    while True:
        job = job_queue.get()
        uspec = job['uspec']
        cluster = job['cluster']  # type: RedshiftCluster
        relation = job['relation']
        schema = uspec['schema']
        s3bucket = uspec['s3bucket']
        dateformat = uspec.get('dateformat')

        ts_start = datetime.now()
        utc_start = local_to_utc(ts_start)

        # Render the s3bucket and s3key using Jinja2. The one/two character
        # variables are convenience shortcuts
        unload_info = {
            'b': s3bucket, 's3bucket': s3bucket,
            'c': cluster.name, 'cluster': cluster.name,
            'db': cluster['database'], 'database': cluster['database'],
            'r': relation, 'relation': relation,
            's': schema, 'schema': schema,
            'ts': ts_start, 'start': ts_start,
            'us': utc_start, 'ustart': utc_start,
            'v': uspec['vars'], 'vars': uspec['vars'],
        }

        # ----------------------------------------
        # Construct the S3 target location
        try:
            s3key = jinja2.Template(uspec['s3key']).render(**unload_info)
        except Exception as e:
            LOG.error('Cluster %s: Unload %s.%s skipped: Bad s3key %s - %s',
                      cluster.name, schema, relation, uspec['s3key'], e)
            job_queue.task_done()
            continue

        # We do not permit single quote in an S3 key name
        if "'" in s3key:
            LOG.error('Cluster %s: Unload %s.%s skipped: Bad s3key %s - single quote(s)',
                      cluster.name, schema, relation, uspec['s3key'])
            job_queue.task_done()
            continue

        unload_info['k'] = unload_info['s3key'] = s3key

        # ----------------------------------------
        # Construct the where clause
        where_predicate = uspec.get('where')
        if where_predicate:
            try:
                where_clause = 'WHERE ' + jinja2.Template(
                    where_predicate
                ).render(**unload_info).replace("'", "\\'")
            except Exception as e:
                LOG.error('Cluster %s: Unload %s.%s skipped: Bad where key %s - %s',
                          cluster.name, schema, relation, where_predicate, e)
                job_queue.task_done()
                continue
        else:
            where_clause = ''

        # ----------------------------------------
        info = None
        action_group = None

        # ----------------------------------------
        try:
            if not conn:
                conn = cluster.connect()

            with conn.cursor() as cursor:

                selector = '*'
                if dateformat:
                    # We have to control date formatting -- see if any columns have dates
                    columns = redshift_get_column_info(cursor, schema, relation)

                    for col in columns:
                        if col[1] == 'date':
                            selector = ', '.join(
                                c[0] if c[1] != 'date' else r'to_char({}, \'{}\')'.format(c[0], dateformat)
                                for c in columns
                            )
                            break

                # --------------------------------
                # Setup the UNLOAD command -- no credentials yet so we can log

                unload_sql = """
                            UNLOAD ('SELECT {selector} FROM {schema}.{relation} {where}')
                            TO 's3://{s3bucket}/{s3key}'
                            {{authorisation}}
                            {params};
                        """.format(
                    selector=selector,
                    schema=schema,
                    where=where_clause,
                    relation=relation,
                    s3bucket=s3bucket,
                    s3key=s3key,
                    params=' '.join(uspec['params'])
                )
                LOG.debug(unload_sql)

                # ------------------------------------
                # Add in credentials - no logging of the query now
                unload_sql = unload_sql.format(authorisation=job['s3cred'])

                # ------------------------------------
                # Check for a dry-run
                if job['dry_run']:
                    LOG.info('Cluster %s: unload %s.%s to %s/%s dry run',
                             cluster.name, schema, relation, s3bucket, s3key)
                    job_queue.task_done()
                    continue
                cursor.execute(unload_sql)
        except Exception as e:
            # Unload failed
            # Replace double quotes with singles for safer JSON encoding in actions
            info = 'UNLOAD failed: {}'.format(e).replace('"', "'")
            action_group = 'on-fail'

            LOG.error('Cluster %s: %s.%s: %s', cluster.name, schema, relation, info)
        else:
            # Unload succeeded.
            LOG.info('Cluster %s: unload %s.%s to %s/%s complete',
                     cluster.name, schema, relation, s3bucket, s3key)
            info = 'UNLOAD complete'
            action_group = 'on-success'
        finally:
            if conn and not reuse_connections:
                try:
                    LOG.debug('Cluster %s: disconnecting', cluster.name)
                    conn.close()
                    conn = None
                except Exception as e:
                    LOG.warning('Cluster %s: connection close error - %s',
                                cluster.name, e)

        # ----------------------------------------
        # Run any post unload actions
        ts_end = datetime.now()
        utc_end = local_to_utc(ts_end)
        duration_s = (ts_end - ts_start).total_seconds()
        duration = round_timedelta(ts_end - ts_start)
        unload_info.update({
            'i': info, 'info': info,
            'te': ts_end, 'end': ts_end,
            'ue': utc_end, 'uend': utc_end,
            'et': duration, 'etime': duration,
            'es': duration_s, 'eseconds': duration_s,
        })

        try:
            do_actions(action_group, uspec.get(action_group, []), unload_info, aws_session)
        except Exception as e:
            LOG.warning('do_action: %s: %s', action_group, str(e))

        # ----------------------------------------
        job_queue.task_done()


# ------------------------------------------------------------------------------
def do_actions(action_key, action_list, unload_info, aws_session=None):
    """
    Execute a list of actions (i.e. things to do after an UNLOAD has succeeded
    or failed.

    :param action_key:      The key in the unlod spec containing the list
                            of actions. Typically 'on-fail' or 'on-success'.
    :params action_list:    A list of action specs. Each action spec is a dict
                            with an action type (e.g. "sqs" for posting to an
                            SQS queue) and parameters specific to the action
                            type.
    :param unload_info:     A dictionaty of info related to the unload operation
                            that gets made available to the action handlers.
    :param aws_session:     A boto3 Session object. If not specified, a default
                            session will be created.

    :type action_key:       str
    :type action_list:      list[dict[str, T]]
    :type aws_session:      boto3.Session
    """

    if not aws_session:
        aws_session = boto3.Session()

    for action_spec in action_list:
        # Find an action handler
        try:
            action = action_spec['action']
            action_handler = getattr(common.actions, 'action_' + action)
        except KeyError:
            LOG.error('%s: "action" key required in all elements of action list', action_key)
            continue
        except AttributeError:
            LOG.error('%s: %s: unknown action', action_key, action_spec['action'])
            continue

        LOG.debug('do_action: %s: %s', action_key, action)

        # Run the action handler
        try:
            action_handler(action_spec, aws_session=aws_session, **unload_info)
        except Exception as e:
            LOG.warning('do_action: %s: %s: %s', action_key, action, e)


# ------------------------------------------------------------------------------
@static_vars(check_results={})
def check_bucket(bucket_name, aws_session=None):
    """
    Do some basic security checks on the S3 bucket. A bucket is only checked
    once (results are cached).

    :param bucket_name:     The S3 bucket name.
    :param aws_session:     A boto3 Session object.

    :type bucket_name:      str
    :type aws_session:      boto3.Session

    :raise Exception:       If the bucket is considered to be insecure.

    """

    # check_results is a dict keyed on bucket name. Values are tuples
    #   (exception raised on last check, time of last check)

    if bucket_name in check_bucket.check_results \
            and (datetime.now() - check_bucket.check_results[bucket_name][1]).total_seconds() < S3_SECURITY_CHECK_AGE:
        # We have checked this one recently
        if check_bucket.check_results[bucket_name][0]:
            # Raise the same exception as we raised previously.
            raise check_bucket.check_results[bucket_name][0]
        else:
            return

    LOG.debug('Checking security of bucket %s', bucket_name)

    # Haven't checked this bucket recently
    try:
        s3_check_bucket_security(bucket_name, aws_session=aws_session)
    except Exception as e:
        # Store the exception so we can replay it if needed
        check_bucket.check_results[bucket_name] = (e, datetime.now())
        raise
    else:
        check_bucket.check_results[bucket_name] = (None, datetime.now())


# ------------------------------------------------------------------------------
def main():
    """
    Starts a bunch of worker threads to perform the unloads, reads the control
    files to get unload specs and then hands them to the workers.

    :raise Exception: If anything goes wrong.
    """

    setup_logging(LOGLEVEL, name=LOGNAME, prefix=PROG, threaded=True)
    args = process_cli_args()
    setup_logging(args.level, name=LOGNAME, target=args.log, colour=args.colour,
                  prefix=args.tag, threaded=True)

    if args.lock_file and not lock_file(args.lock_file):
        raise Exception('Cannot get a lock - is another instance already running?')

    aws_session = boto3.Session(profile_name=args.profile)
    cluster_table = ClusterTable(args.cluster_table, aws_session=aws_session)

    # ----------------------------------------
    # Prepare for threading - create job queue and start worker threads. The
    # main thread feeds unload jobs to the workers via a queue.

    # job_queue = Queue(maxsize=args.threads)
    job_queue = Queue()
    for t in range(args.threads):
        worker = Thread(
            target=unload_worker,
            name='thread-{:02}'.format(t),
            kwargs={
                'job_queue': job_queue,
                'reuse_connections': REUSE_UNLOAD_CONNECTIONS,
                'aws_session': aws_session
            }
        )
        worker.daemon = True
        worker.start()

    # ----------------------------------------
    unloads = set()  # Try to avoid unloading same thing twice

    for cf in args.control_files:
        try:
            control = ControlFile(cf)
            cluster = cluster_table[control.cluster]
        except Exception as e:
            LOG.error('%s: %s', cf, e)
            continue

        if not cluster.active:
            LOG.warning('%s: Cluster %s is not active', cf, cluster.name)
            continue

        try:
            n = 0
            for uspec in control.uspecs:
                n += 1

                # Check bucket security
                if not args.insecure:
                    try:
                        check_bucket(uspec['s3bucket'], aws_session)
                    except Exception as e:
                        LOG.error('%s: Skipping unload spec %d: %s - %s',
                                  cf, n, uspec['s3bucket'], e)
                        continue

                # Expand the list of relations to unload
                schema = uspec['schema'].lower()
                for relation in cluster.relations_in_schema(
                        schema=schema,
                        include=mget(uspec, USPEC_SELECTOR_KEYS),
                        exclude=uspec.get('exclude'),
                        tables='tables' in uspec or 'relations' in uspec,
                        views='views' in uspec or 'relations' in uspec,
                        conn_age=MAIN_CONNECTIONS_AGE
                ):
                    # Fully qualified relation name
                    fq_relation = (cluster.name, schema, relation.lower())
                    if fq_relation in unloads:
                        LOG.info('%s: Skipping %s.%s.%s - already included', cf, *fq_relation)
                        continue

                    job_queue.put(
                        {
                            'cluster': cluster,
                            's3cred': control.s3cred,
                            'uspec': uspec,
                            'relation': relation,
                            'dry_run': args.dry_run,
                        }
                    )
                    unloads.add(fq_relation)
        except Exception as e:
            raise Exception('{}: {}'.format(cf, e))

    # ----------------------------------------
    # All done -- wait for unload workers to finish.

    LOG.debug('Waiting for worker threads to complete their work before finishing')
    job_queue.join()

    # Allow the daemon threads to finish before destroying everything
    time.sleep(1)
    return 0


# ------------------------------------------------------------------------------
if __name__ == '__main__':
    # exit(main())  # Uncomment for debugging
    try:
        exit(main())
    except Exception as ex:
        LOG.error('%s', ex)
        exit(1)
    except KeyboardInterrupt:
        LOG.warning('Interrupt')
        exit(2)
