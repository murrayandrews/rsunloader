#!/usr/bin/env python3
"""
Automated RedShift Extractor With Inline Parameter Expansion - Daemon Version

... aka Redshift Unloader Daemon (rsud).

The is a tool for bulk unloading data from Redshift clusters to S3. When coupled
with rsDropLoader2, it allows bulk transfer of data between clusters.

This is based on the rsu code but instead of reading unload specs from a control
file, it receives them on an SQS queue. It can operate in either batch or daemon
modes.

The evworker code has been repurposed for the control framework. It could have
been written as an evw plugin but hasn't for these reasons:

1.  Unloads very often take longer than the 5 minutes allowed by Lambda so it
    was only ever likely to be used as an evworker daemon plugin.

2.  Unloads are very resource intensive on Redshift clusters. Running in Lambda
    makes it much more difficult to limit the number of simultaneous unloads
    whereas this is easy with the evworker framework by controlling thread
    count.

3.  I couldn't be bothered. Basically this implememtation is just lipstick on
    the existing rsu pig.

-- MA

................................................................................
Copyright (c) 2018, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

# ..............................................................................
# region imports

from __future__ import print_function

import argparse
import json
import platform
import time
from datetime import datetime, timedelta
from threading import Thread

from common.logging import setup_logging
from common.mu_daemon import daemonify
from common.utils import lock_file, mget, dict_check
from rsu import CONTROL_REQUIRED_KEYS, PROG, CLUSTER_TABLE, \
    LOG, LOGLEVEL, LOGNAME, \
    THREADS, ClusterTable, ControlFile, unload_worker, \
    check_bucket, USPEC_SELECTOR_KEYS
from rsu import __version__ as rsu_version

try:
    # Python 2
    # noinspection PyUnresolvedReferences, PyCompatibility
    from Queue import Queue
except ImportError:
    # Python 3
    # noinspection PyUnresolvedReferences, PyCompatibility
    from queue import Queue

import boto3

# endregion imports
# ..............................................................................

# ..............................................................................
# region constants

__author__ = 'Murray Andrews'
__version__ = '1.1.0 (rsu v{})'.format(rsu_version)

HEARTBEAT_MIN = 30  # Minimum time between heartbeat messages

LONG_POLL_DEFAULT = 10  # Wait 10 seconds for SQS messages to arrive
LONG_POLL_MAX = 20  # Max wait time
SQS_POLL_SLEEP = 10  # Sleep between SQS polls when no messages available.
BUSY_SLEEP = 5  # Sleep for this many seconds if all threads busy.
RETRIES = 2  # No. retries to process an unload request before giving up on it.

# For rsud the unload specs come from SQS
CONTROL_REQUIRED_KEYS.remove('unload')

# These constants control reuse of connections to Redshift. Even with reuse
# enabled, connections are not shared across threads.
MAIN_CONNECTIONS_AGE = 10 * 60  # Main thread can hold connection for 10 mins
REUSE_UNLOAD_CONNECTIONS = False


# endregion constants
# ..............................................................................


# ------------------------------------------------------------------------------
def process_cli_args():
    """
    Process the command line arguments.
    :return:    The args namespace.
    """

    argp = argparse.ArgumentParser(
        prog=PROG,
        description='Redshift table unloader'
    )

    argp.add_argument('-b', '--heartbeat', action='store', type=int, default=0,
                      help='Emit a heartbeat log message every this many seconds.'
                           ' A value of 0 (the default) disables heartbeats. If'
                           ' specified, a minimum of {} seconds is imposed.'.format(HEARTBEAT_MIN))

    run_mode_group = argp.add_argument_group(
        'run mode arguments',
        description='If none of the following are specified, run in the foreground.'
    )

    run_mode_args = run_mode_group.add_mutually_exclusive_group()

    run_mode_args.add_argument('--batch', action='store_true',
                               help='Run a single batch and exit when queue is empty.')

    argp.add_argument('-c', '--no-colour', '--no-color', dest='colour', action='store_false',
                      default=True, help='Don\'t use colour in information messages.')

    argp.add_argument('--cluster-table', dest='cluster_table', action='store',
                      default=CLUSTER_TABLE,
                      help='Name of the DynamoDB table containing connection'
                           ' information for Redshift clusters. Default is'
                           ' "{}".'.format(CLUSTER_TABLE))

    argp.add_argument('--dry-run', dest='dry_run', action='store_true',
                      help='Do a dry run. No unloads are performed but relations'
                           ' that would be unloaded are identified.')

    run_mode_args.add_argument('-d', '--daemon', action='store_true',
                               help='Run as a daemon.')

    argp.add_argument('--insecure', action='store_true',
                      help='Allow writing to insecure buckets. By default, some'
                           ' *basic* security checks are performed on the bucket'
                           ' and the unload process to prevent accidents.')

    argp.add_argument('-l', '--level', metavar='LEVEL', default=LOGLEVEL,
                      help='Print messages of a given severity level or above.'
                           ' The standard logging level names are available but info,'
                           ' warning and error are most useful. Default is {}.'.format(LOGLEVEL))

    argp.add_argument('--lock-file', dest='lock_file', action='store',
                      help='Create a lock file with the given name to prevent'
                           ' two instances using the same lock file from running'
                           ' simultaneously.')
    argp.add_argument('--log', action='store',
                      help='Log to the specified target. This can be either a file'
                           ' name or a syslog facility with an @ prefix (e.g. @local0).')

    argp.add_argument('--profile', action='store', help='As for AWS CLI.')

    argp.add_argument('-q', '--queue', action='store',
                      help='AWS SQS queue name. If specified, overrides any value'
                           ' in the control file.')

    argp.add_argument('-r', '--retries', action='store', type=int, default=RETRIES,
                      help='Maximum number of retry attempts to process an event.'
                           ' If an event is not processed after this many retries,'
                           ' it is discarded. Note that it is possible for an event'
                           ' to be processed more than once. If set to a negative'
                           ' number, retry limiting is disabled. Default {}.'.format(RETRIES))

    argp.add_argument('-s', '--sleep', action='store',
                      type=int, default=SQS_POLL_SLEEP,
                      help='Sleep for this many seconds between SQS poll attempts'
                           ' when no messages are available. Note that the -w/--wait'
                           ' time is outside this sleep time. Default {}.'.format(SQS_POLL_SLEEP))

    argp.add_argument('-t', '--threads', action='store', type=int, default=THREADS,
                      help='Run this many threads. Default {}.'.format(THREADS))

    argp.add_argument('--tag', action='store', default=PROG,
                      help='Tag log entries with the specified value. The default is {}.'.format(PROG))

    argp.add_argument('-v', '--version', action='version', version=__version__,
                      help='Print version and exit.')

    argp.add_argument('-w', '--wait', action='store', type=int, default=LONG_POLL_DEFAULT,
                      help='Wait this many seconds for the SQS queue to provide messages'
                           ' (long polling). Must be in the range 0 .. {}. Default is'
                           ' {} seconds.'.format(LONG_POLL_MAX, LONG_POLL_DEFAULT)
                      )

    argp.add_argument('control_file', metavar='control-file',
                      help='Control file (prefix with s3:// for S3 based file.')

    args = argp.parse_args()

    if not 0 <= args.wait <= LONG_POLL_MAX:
        raise ValueError(
            'Argument -w/--wait: value must be in the range 0 .. {}'.format(LONG_POLL_MAX))

    if args.heartbeat and args.heartbeat < HEARTBEAT_MIN:
        raise ValueError(
            'Argument -b/--heartbeat: value must be 0 or greater than {}'.format(HEARTBEAT_MIN))

    return args


# ------------------------------------------------------------------------------
def heartbeat(job_queue, sqs_queue, hb_period):
    """
    Thread worker to issue heartbeat messages.

    :param job_queue:   An internal (Python) queue containing unload jobs.
    :param sqs_queue:   SQS queue from which events are being loaded.
    :param hb_period:   How often heartbeat messages are issued.

    :type job_queue:  Queue
    :type hb_period:    int

    """

    LOG.debug('Starting')
    assert hb_period > 0, 'Bad heartbeat period: {}'.format(hb_period)

    wake_time = datetime.utcnow()

    sqs_q_arn = sqs_queue.attributes['QueueArn']
    host = platform.node()

    while True:
        wake_time += timedelta(seconds=hb_period)
        try:
            sqs_queue.load()
        except Exception as e:
            LOG.warning(f'Could not get attributes on {sqs_q_arn}: {e}')
            sqs_q_len = '??'
            sqs_q_nvis = '??'
        else:
            sqs_q_len = sqs_queue.attributes['ApproximateNumberOfMessages']
            sqs_q_nvis = sqs_queue.attributes['ApproximateNumberOfMessagesNotVisible']

        now = datetime.utcnow()
        LOG.info(
            f'{now.isoformat()} {host} {sqs_q_arn} len={sqs_q_len}'
            f' notvisibile={sqs_q_nvis} Internal len={job_queue.qsize()}'
        )

        try:
            time.sleep((wake_time - now).total_seconds())
        except ValueError as e:
            LOG.warning(str(e))


# ------------------------------------------------------------------------------
def main():
    """
    Kicks off a bunch of unloader threads then receives messages from the SQS
    queue, analyse them to extract details of relations to unload and then feed
    these to the unloader threads. Loops endlessly if not in batch mode. In
    batch mode, exits once the SQS queue is empty.

    :raise Exception: If anything goes wrong.
    """

    setup_logging(LOGLEVEL, name=LOGNAME, prefix=PROG, threaded=True)
    args = process_cli_args()

    if args.daemon:
        daemonify(pidfile=args.lock_file)
    elif args.lock_file and not lock_file(args.lock_file):
        raise Exception('Cannot get a lock - is another instance already running?')

    setup_logging(args.level, name=LOGNAME, target=args.log, colour=args.colour,
                  prefix=args.tag, threaded=True)

    aws_session = boto3.Session(profile_name=args.profile)
    cluster_table = ClusterTable(args.cluster_table, aws_session=aws_session)

    # ----------------------------------------
    # Read the control file
    control = ControlFile(args.control_file)
    cluster = cluster_table[control.cluster]

    try:
        # Only allow limited keys in unload requests
        uspec_allowed_keys = control['uspec']
    except KeyError:
        uspec_allowed_keys = None  # Allow all the standard keys

    # ----------------------------------------
    # Acquire the SQS queue

    if args.queue:
        queue_name = args.queue
    else:
        try:
            queue_name = control['queue']
        except KeyError:
            raise Exception('SQS queue name not specified')

    LOG.debug('Getting SQS queue %s', queue_name)
    sqs_queue = aws_session.resource('sqs').get_queue_by_name(QueueName=queue_name)
    LOG.info('SQS queue %s', sqs_queue.url)

    job_queue = Queue(maxsize=args.threads)

    # ----------------------------------------
    # Start the heartbeat thread
    if args.heartbeat:
        worker = Thread(
            target=heartbeat,
            name='heartbeat',
            args=(job_queue, sqs_queue, args.heartbeat)
        )
        worker.daemon = True
        worker.start()

    # ----------------------------------------
    # Start the worker threads
    for t in range(args.threads):
        worker = Thread(
            target=unload_worker,
            name='thread-{:02}'.format(t),
            kwargs={
                'job_queue': job_queue,
                'reuse_connections': REUSE_UNLOAD_CONNECTIONS,
                'aws_session': aws_session
            }
        )
        worker.daemon = True
        worker.start()

    # ----------------------------------------
    # Poll for messages on the SQS queue and place them on the internal worker queue
    sleep_time = max(0, args.sleep)
    while True:
        if job_queue.full():
            # Don't retrieve messages from SQS if our internal worker queue is full
            time.sleep(BUSY_SLEEP)
            continue

        LOG.debug('Polling SQS')
        sqs_messages = sqs_queue.receive_messages(
            AttributeNames=['All'],  # We don't really use this info
            MessageAttributeNames=['All'],
            WaitTimeSeconds=args.wait,
            MaxNumberOfMessages=1
        )
        if sqs_messages:
            for msg in sqs_messages:
                # ----------------------------
                # Check how many attempts with this request.
                attempt_number = int(msg.attributes['ApproximateReceiveCount'])
                if args.retries >= 0 and attempt_number > args.retries + 1:
                    LOG.warning('Message %s: Retry limit exceeded - deleted on cycle %d',
                                msg.message_id, attempt_number)
                    msg.delete()
                    continue

                # ----------------------------
                # Decode the request.
                try:
                    msg_body = json.loads(msg.body)
                except Exception as e:
                    LOG.error('Message %s: Cannot decode body - %s',
                              msg.message_id, e)
                    msg.delete()
                    continue

                LOG.debug('Unload request: %s', msg_body)

                # ----------------------------
                # Check request only has allowed keys. Other components must
                # come from defaults in the control file.
                if not isinstance(msg_body, list):
                    msg_body = [msg_body]

                uspecs = []
                if uspec_allowed_keys:
                    n = 0
                    for u in msg_body:
                        n += 1
                        try:
                            dict_check(u, optional=uspec_allowed_keys)
                        except ValueError as e:
                            LOG.error('Message %s: Skipping unload spec %d: %s',
                                      msg.message_id, n, e)
                        else:
                            uspecs.append(u)

                # ----------------------------
                # Store the unload specs. This merges in the control file
                # defaults.
                try:
                    control.uspecs = uspecs
                except Exception as e:
                    LOG.error('Message %s: %s', msg.message_id, e)
                    msg.delete()
                    continue

                # Try to avoid unloading same thing twice in a single request.
                unloads = set()

                # ----------------------------
                try:
                    n = 0
                    for uspec in control.uspecs:
                        n += 1

                        # Check bucket security
                        if not args.insecure:
                            try:
                                check_bucket(uspec['s3bucket'], aws_session)
                            except Exception as e:
                                LOG.error('Message %s: Skipping unload spec %d: %s - %s',
                                          msg.message_id, n, uspec['s3bucket'], e)
                                continue

                        # Expand the list of relations to unload
                        for relation in cluster.relations_in_schema(
                                schema=uspec['schema'],
                                include=mget(uspec, USPEC_SELECTOR_KEYS),
                                exclude=uspec.get('exclude'),
                                tables='tables' in uspec or 'relations' in uspec,
                                views='views' in uspec or 'relations' in uspec,
                                conn_age=MAIN_CONNECTIONS_AGE
                        ):
                            if relation in unloads:
                                LOG.debug('Message %s: Skipping %s - already included',
                                          msg.message_id, relation)
                                continue

                            job_queue.put(
                                {
                                    'cluster': cluster,
                                    's3cred': control.s3cred,
                                    'uspec': uspec,
                                    'relation': relation,
                                    'dry_run': args.dry_run,
                                }
                            )
                            unloads.add(relation)
                except Exception as e:
                    LOG.error('Message %s: %s', msg.message_id, e)
                finally:
                    msg.delete()
        elif args.batch:
            LOG.info('No more messages in batch -- finishing')
            break
        elif sleep_time:
            LOG.debug('SQS poll sleep %d', sleep_time)
            time.sleep(sleep_time)

    LOG.debug('Waiting for worker threads to complete their work before finishing')
    job_queue.join()


# ------------------------------------------------------------------------------
if __name__ == '__main__':
    # exit(main())  # Uncomment for debugging
    try:
        exit(main())
    except Exception as ex:
        LOG.error('%s', ex)
        exit(1)
    except KeyboardInterrupt:
        LOG.warning('Interrupt')
        exit(2)
