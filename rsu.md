# rsu(1) -- Redshift Unloader


## SYNOPSIS

`rsu` \[options\] control-file ...

## DESCRIPTION

**Rsu** automates the process of unloading data from Redshift to AWS S3.  It is
multi-threaded and designed to simplify the process of unloading lots of tables
and views in a single batch.

## OPTIONS

*   _control-file_:
    A YAML formatted control file that species which relations (tables and
    views) should be unloaded, to which S3 location and in what format. Each
    control file is dedicated to a single Redshift cluster. A control file name
    starting with _s3://_ indicates the file is to be downloaded from S3.

*   `-c`, `--no-colour`, `--no-color`:
    Don't use colour in information messages.

*   `--cluster-table` _CLUSTER-TABLE_:
    **Rsu** reads connectivity information for Redshift clusters from a
    DynamoDB table. This option specifies the name of the DynamoDB
    table. If not specified the value of the <RSUNLOAD_CLUSTER_TABLE> environment
    variable is used. If this is not set the default is <clusters>.

*   `--dry-run`:
    Do a dry run. No unloads are performed but relations that would be unloaded
    are identified.

*   `-h`:
    Print help and exit.

*   `--insecure`:
    Allow writing to insecure S3 buckets. By default, some BASIC security
    checks are performed on the bucket to reduce the risk of accidents but DO
    NOT rely on this for security.


*   `-l` _LEVEL_, `--level` _LEVEL_:
    Print messages of a given severity level or above. The standard logging
    level names are available but _debug_, _info_, _warning_ and _error_ are
    most useful.  The Default is _info_.

*   `--lock-file` _LOCK-FILE_:
    Create a lock file with the given name to prevent two instances using the
    same lock file from running simultaneously.

*   `--log` _LOG_:
    Log to the specified target. This can be either a file name or a syslog
    facility with an @ prefix (e.g. _@local0_).

*   `--profile` _profile_:
    As for the AWS CLI **--profile** option.

*   `-t` _THREADS_, `--threads` _THREADS_:
    Run this many threads. Default 2.

*   `--tag` _TAG_:
    Tag log entries with the specified value. The default is <rsu>.

*   `-v`, `--version`:
    Print the version and exit.


## ENVIRONMENT

*   <RSUNLOAD_CLUSTER_TABLE>:
    Optionally specifies the name of the DynamoDB table containing
    cluster connectivity information.


## SEE ALSO

[rsunloader](https://bitbucket.org/murrayandrews/rsunloader) on Bitbucket.

## AUTHOR

Murray Andrews

## LICENCE

[BSD 3-clause licence](http://opensource.org/licenses/BSD-3-Clause).
