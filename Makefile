# Makefile for rsUnloader.
# 
# Try:
# 	make help

PKG_FILES=$(wildcard rsu.*) LICENCE.md README.md requirements.txt common Makefile
PKG1_MODULES=jinja2 markupsafe pg8000 yaml six passlib scramp
PKG2_MODULES=boto3 botocore jmespath dateutil s3transfer $(PKG1_MODULES)
PKG_EXCLUDES=*.pyc __pycache__ *.zip *.swp *.tar.*

# ..............................................................................
TMPDIR=tmp_build
VENV_LIB=venv/lib/python3.6/site-packages

#MD_TO_MAN = ronn --roff --organization MA --manual AWStools --date `date +'%Y-%m-%d'`
MD_TO_MAN = ronn --roff --organization MA --manual AWStools

LOCAL_MANFILES=rsu.1 rsud.1
MAN_SECTION=1
MANFILES=$(addprefix $(MAN)/man$(MAN_SECTION)/,$(LOCAL_MANFILES))

.PHONY: man clean clobber rsu rsu1 rsu2 _pkg
# ..............................................................................
#$(MAN)/man*/%: %
#	$(INSTALL) -m $(MANMODE) $< $@

%.$(MAN_SECTION): %.md
	$(MD_TO_MAN) $<

help:
	@echo What do you want to make?
	@echo
	@echo "    man:	Make the man page"
	@echo "    clean:	Remove some fluff."
	@echo "    clobber:	Remove even more fluff."
	@echo "    rsu:	Create a tar file containing the code."
	@echo "    rsu1:	Create a tar file containing the code and all modules apart"
	@echo "    		from boto3 and friends. The result should be executable on"
	@echo "    		any compatible system that has boto3 installed centrally."
	@echo "    rsu2:	Create a tar file containing the code and all modules. The"
	@echo "    		result should be executable on any compatible system without"
	@echo "    		depending on any non-standard Python modules."
	@echo


man:	$(LOCAL_MANFILES)  # Create man files locally

clean:	
	$(RM) *.tar.bz2
	$(RM) -r __pycache__ common/__pycache__

clobber: clean
	$(RM) $(LOCAL_MANFILES)

# ..............................................................................
# Just the code - no extra modules.
rsu:
	@$(MAKE) _pkg MODULES= PKG=$@

# ..............................................................................
# Code + non-boto3 modules
rsu1:
	@$(MAKE) _pkg MODULES="$(PKG1_MODULES)" PKG=$@

# ..............................................................................
# Code + with all required non-standard modules.
rsu2:
	@$(MAKE) _pkg MODULES="$(PKG2_MODULES)" PKG=$@

# ==============================================================================
# Internal use only
_pkg:
	@mkdir -p $(TMPDIR)
	@cp -r $(PKG_FILES) $(TMPDIR) && echo + $(PKG_FILES)
	@for m in $(MODULES) ; \
	do \
		module=$(VENV_LIB)/$$m ; \
		[ ! -f $$module.py -a ! -d $$module ] && echo "*** Missing package: $$m" ; \
		[ -f $$module.py ] && cp $$module.py $(TMPDIR) && echo + $$m ; \
		[ -d $$module ] && cp -r $$module $(TMPDIR) && echo + $$m ; \
	done || :
	@( \
		cd $(TMPDIR);  \
		tar cjf $(CURDIR)/$(PKG).tar.bz2 $(foreach e,$(PKG_EXCLUDES),--exclude $(e)) * ; \
	)
	@/bin/ls -l $(PKG).tar.bz2
	@$(RM) -r $(TMPDIR)
