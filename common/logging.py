"""

Copyright (c) 2017, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

import logging
import os
import stat
import sys
from logging.handlers import SysLogHandler

__author__ = 'Murray Andrews'

# ..............................................................................
# region colour

# ------------------------------------------------------------------------------
# Clunky support for colour output if colorama is not installed.

try:
    # noinspection PyUnresolvedReferences
    import colorama
    # noinspection PyUnresolvedReferences
    from colorama import Fore, Style

    colorama.init()

except ImportError:
    class Fore(object):
        """
        Basic alternative to colorama colours using ANSI sequences
        """
        RESET = '\033[0m'
        BLACK = '\033[30m'
        RED = '\033[31m'
        GREEN = '\033[32m'
        YELLOW = '\033[33m'
        BLUE = '\033[34m'
        MAGENTA = '\033[35m'
        CYAN = '\033[36m'


    class Style(object):
        """
        Basic alternative to colorama styles using ANSI sequences
        """
        RESET_ALL = '\033[0m'
        BRIGHT = '\033[1m'
        DIM = '\033[2m'
        NORMAL = '\033[22m'


# endregion colour
# ..............................................................................


# ..............................................................................
# region logging

# -------------------------------------------------------------------------------
def syslog_address():
    """
    Try to work out the syslog address.

    :return:    A value suitable for use as the address arg for SysLogHandler.
    :rtype:     tuple | str
    """

    for f in ('/dev/log', '/var/run/syslog'):
        try:
            mode = os.stat(f).st_mode
        except OSError:
            continue

        if stat.S_ISSOCK(mode):
            return f

    return 'localhost', 514


# -------------------------------------------------------------------------------
def get_log_level(s):
    """
    Convert the string version of a log level defined in the logging module to the
    corresponding log level. Raises ValueError if a bad string is provided.

    :param s:       A string version of a log level (e.g. 'error', 'info').
                    Case is not significant.
    :type s:        str

    :return:        The numeric logLevel equivalent.
    :rtype:         int

    :raises:        ValueError if the supplied string cannot be converted.
    """

    if not s or not isinstance(s, str):
        raise ValueError('Bad log level:' + str(s))

    t = s.upper()

    if not hasattr(logging, t):
        raise ValueError('Bad log level: ' + s)

    return getattr(logging, t)


# ------------------------------------------------------------------------------
class ColourLogHandler(logging.Handler):
    """
    Basic stream handler that writes to stderr but with different colours for
    different message levels.

    """

    # --------------------------------------------------------------------------
    def __init__(self, colour=True):
        """
        Allow colour to be enabled or disabled.

        :param colour:      If True colour is enabled for log messages.
                            Default True.
        :type colour:       bool

        """

        super(ColourLogHandler, self).__init__()
        self.colour = colour

    # --------------------------------------------------------------------------
    def emit(self, record):
        """
        Print the record to stderr with some colour enhancement.

        :param record:  Log record
        :type record:   logging.LogRecord
        :return:
        """

        if self.colour:
            if record.levelno >= logging.ERROR:
                colour = Style.BRIGHT + Fore.RED
            elif record.levelno >= logging.WARNING:
                colour = Fore.MAGENTA
            elif record.levelno >= logging.INFO:
                colour = Fore.BLUE
            else:
                colour = Style.DIM + Fore.BLACK

            print(colour + self.format(record) + Fore.RESET + Style.RESET_ALL, file=sys.stderr)
        else:
            print(self.format(record), file=sys.stderr)


# ------------------------------------------------------------------------------
LOG_FORMATS = {
    'syslog': '[%(process)d]: %(levelname)s: %(message)s',
    'syslog_threaded': '[%(process)d]: %(levelname)s: %(threadName)s: %(message)s',
    'file': '%(asctime)s: %(levelname)s: %(message)s',
    'file_threaded': '%(asctime)s: %(levelname)s: %(threadName)s: %(message)s',
    'stderr': '%(message)s',
    'stderr_threaded': '%(threadName)s: %(message)s',
}


# ------------------------------------------------------------------------------
def setup_logging(level, target=None, colour=True, name=None, prefix=None,
                  threaded=False):
    """
    Setup logging.

    :param level:   Logging level. The string format of a level (eg 'debug').
    :param target:  Logging target. Either a file name or a syslog facility name
                    starting with @ or None. If None, log to stderr.
    :param colour:  If True and logging to the terminal, colourise messages for
                    different logging levels. Default True.
    :param name     The name of the logger to configure. If None, configure the
                    root logger.
    :param prefix:  Messages a prefixed by this string (with colon+space
                    appended). Default None.
    :param threaded: If True, the thread name will be included in log messages.
                    Default False.
    :type level:    str
    :type target    str | None
    :type colour:   bool
    :type prefix:   str | None
    :type name:     str | None
    :type threaded: bool

    :raise ValueError: If an invalid log level or syslog facility is specified.
    """

    logger = logging.getLogger(name)
    logger.propagate = False
    logger.setLevel(get_log_level(level))

    # Get rid of unwanted handlers.
    for h in logger.handlers:
        logger.removeHandler(h)

    if target:
        if target.startswith('@'):
            # Syslog to specified facility

            if target[1:] not in SysLogHandler.facility_names:
                raise ValueError('Bad syslog facility: {}'.format(target[1:]))
            h = SysLogHandler(address=syslog_address(), facility=target[1:])
            h.setFormatter(
                logging.Formatter(
                    (prefix if prefix else '') + LOG_FORMATS['syslog_threaded' if threaded else 'syslog']
                )
            )
        else:
            # Log to a file
            h = logging.FileHandler(target)
            h.setFormatter(logging.Formatter(LOG_FORMATS['file_threaded' if threaded else 'file']))
        logger.addHandler(h)
        logger.debug('%s', ' '.join(sys.argv))
        logger.debug('Logfile set to %s', target)
    else:
        # Just log to stderr.
        h = ColourLogHandler(colour=colour)
        h.setFormatter(logging.Formatter(
            (prefix + ': ' if prefix else '') + LOG_FORMATS['stderr_threaded' if threaded else 'stderr']))
        logger.addHandler(h)
    logger.debug('Log level set to %s (%d)', level, logger.getEffectiveLevel())

# endregion logging
# ..............................................................................
