"""
This module contains "action" handlers that perform some kind of action as
defined by an action spec. Typical actions are:

    - Post to an SQS queue
    - Post to an SNS topic
    - Send an email via SES

................................................................................
Copyright (c) 2017, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

import json
from datetime import datetime

import boto3
import jinja2
from botocore.client import Config

from .utils import dict_check, json_default

__author__ = 'Murray Andrews'

SES_REGION = 'us-east-1'
SES_SENDER_NAME = 'no-reply'
SES_SENDER_SUBDOMAIN = 'event'
SES_SENDER_DOMAIN = None  # If None, try to find a verified domain and use that.


# ----------------------------------------------------------------------------
def action_sqs(action_spec, aws_session=None, **kwargs):
    """
    Action handler to send an SQS message. Keys required in action_spec are:

    - queue:    The name or URL for the queue.

    - message:  Either a string or An object that will be JSON encoded and
                sent as the SQS message. It is rendered using Jinja2 with
                the kwargs being injected.

    :param action_spec: A dictionary of parameters for this action.
    :param aws_session: A boto3 Session object. If not specified, a default
                        session will be created.
    :param kwargs:      An arbitrary set of parameters that are passed to the
                        jinja2 renderer.

    :type action_spec:  dict[str, T]
    :type aws_session:  boto3.Session

    """

    dict_check(action_spec, required=('queue', 'message'))

    if not aws_session:
        aws_session = boto3.Session()

    if '.amazonaws.com/' in action_spec['queue']:
        # Assume its a URL
        sqs_queue = aws_session.resource('sqs').Queue('https://' + action_spec['queue'])
    else:
        sqs_queue = aws_session.resource('sqs').get_queue_by_name(QueueName=(action_spec['queue']))

    message = action_spec['message'] if isinstance(action_spec['message'], str) \
        else json.dumps(action_spec['message'], default=json_default)

    sqs_queue.send_message(
        MessageBody=jinja2.Template(message).render(**kwargs)
    )


# ----------------------------------------------------------------------------
def action_sns(action_spec, aws_session=None, **kwargs):
    """
    Action handler to send an SNS message. The required keys are:

    - topic:    The topic ARN.

    - message:  Either a string or an object that will be JSON encoded and
                sent as the SNS message. It is rendered using Jinja2 with
                the kwargs being injected.

    Optional keys:

    - subject:  Message subject string. It is rendered using Jinja2 with the
                kwargs being injected.

    :param action_spec: A dictionary of parameters for this action.
    :param aws_session: A boto3 Session object. If not specified, a default
                        session will be created.
    :param kwargs:      An arbitrary set of parameters that are passed to the
                        jinja2 renderer.

    :type action_spec:  dict[str, T]
    :type aws_session:  boto3.Session
    """

    dict_check(action_spec, required=('topic', 'message'))

    if not aws_session:
        aws_session = boto3.Session()

    sns = aws_session.client('sns', config=Config(signature_version='s3v4'))

    message = action_spec['message'] if isinstance(action_spec['message'], str) \
        else json.dumps(action_spec['message'], default=json_default)

    sns_args = {
        'TopicArn': action_spec['topic'],
        'Message': jinja2.Template(message).render(**kwargs)
    }

    try:
        sns_args['Subject'] = jinja2.Template(action_spec['subject']).render(**kwargs)
    except KeyError:
        pass

    sns.publish(**sns_args)


# ------------------------------------------------------------------------------
def action_ses(action_spec, aws_session=None, **kwargs):
    """
    Action handler to send an SES email message.  Send an email via SES. Note
    that SES is not available in all regions so we force us-east-1.

    The required keys are:

    - to:       The recipient email address or a list of addresses. Each must
                be an SES verified email.

    - subject:  Message subject string. It is rendered using Jinja2 with the
                kwargs being injected.

    - message:  Message body. It is rendered using Jinja2 with
                the kwargs being injected.

    Optional keys: None

    :param action_spec: A dictionary of parameters for this action.
    :param aws_session: A boto3 Session object. If not specified, a default
                        session will be created.
    :param kwargs:      An arbitrary set of parameters that are passed to the
                        jinja2 renderer.

    :type action_spec:  dict[str, T]
    :type aws_session:  boto3.Session

    """

    global SES_SENDER_DOMAIN

    dict_check(action_spec, required=('to', 'subject', 'message'))

    if not aws_session:
        aws_session = boto3.Session()

    ses = aws_session.client('ses', region_name=SES_REGION)

    if not SES_SENDER_DOMAIN:
        identities = ses.list_identities(IdentityType='Domain')['Identities']
        if not identities:
            raise Exception('No verified domains - cannot use AWS SES')

        # Just pick the first one and subdomain it.
        SES_SENDER_DOMAIN = SES_SENDER_SUBDOMAIN + '.' + identities[0]

    to_list = [action_spec['to']] if isinstance(action_spec['to'], str) else action_spec['to']

    ses.send_email(
        Source=SES_SENDER_NAME + '@' + SES_SENDER_DOMAIN,
        Destination={
            'ToAddresses': to_list
        },
        Message={
            'Subject': {
                'Data': jinja2.Template(action_spec['subject']).render(**kwargs),
                'Charset': 'UTF-8'
            },
            'Body': {
                'Text': {
                    'Data': jinja2.Template(action_spec['message']).render(**kwargs),
                    'Charset': 'UTF-8'
                }
            }
        }
    )


# ------------------------------------------------------------------------------
def action_cwm(action_spec, aws_session=None, **kwargs):
    """
    Action handler to post custom Cloudwatch metrics.

    The required keys are:

    - namespace:    The Cloudwatch namespace.
    - dimensions:   A dictionary of metric dimensions.
    - metrics:      A list of metric specifications.

    Optional keys:

    - timestamp:    The key in kwargs that contains the timestamp to use.
                    Default is current time. This is NOT jinja2 rendered.

    The metric specifications have the following keys:

    - name:         The metric name. Required.
    - value:        The metric value. Required.
    - unit:         The metric unit (as specified by CloudWatch). Optional.

    :param action_spec: A dictionary of parameters for this action.
    :param aws_session: A boto3 Session object. If not specified, a default
                        session will be created.
    :param kwargs:      An arbitrary set of parameters that are passed to the
                        jinja2 renderer.

    :type action_spec:  dict[str, T]
    :type aws_session:  boto3.Session

    """

    dict_check(
        action_spec,
        required={'action', 'namespace', 'dimensions', 'metrics'},
        optional={'timestamp'}
    )

    # Make sure the metric entries contain the required keys
    metrics = action_spec['metrics']
    for m in metrics:
        dict_check(m, required={'name', 'value'}, optional={'unit'})

    if not aws_session:
        aws_session = boto3.Session()

    cloudwatch = aws_session.client('cloudwatch')

    # ----------------------------------------
    # Render the dimensions. Transform a dictionary of dimensions into the
    # form required by Cloudwatch.
    dimensions = [
        {
            'Name': d,
            'Value': jinja2.Template(action_spec['dimensions'][d]).render(**kwargs)
        } for d in sorted(action_spec['dimensions'])
    ]

    # ----------------------------------------
    # Render and collect the metrics
    metric_data = [
        {
            'MetricName': m['name'],
            'Dimensions': dimensions,
            'Timestamp': kwargs[action_spec['timestamp']] if 'timestamp' in action_spec else datetime.utcnow(),
            'Value': float(jinja2.Template(m['value']).render(**kwargs) if isinstance(m['value'], str) else m['value']),
            'Unit': m['unit'].capitalize() if 'unit' in m else 'None'
        } for m in metrics
    ]

    # ----------------------------------------
    # Send it to Cloudwatch
    cloudwatch.put_metric_data(
        Namespace=jinja2.Template(action_spec['namespace']).render(**kwargs),
        MetricData=metric_data
    )
