"""
AWS related utilities.

Copyright (c) 2016, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
import json
from base64 import b64decode

import boto3
from botocore.exceptions import ClientError
import yaml
from .utils import sepjoin
from common.decorators import static_vars

__author__ = 'Murray Andrews'


# ..............................................................................
# region S3 utils.
# ..............................................................................


# ------------------------------------------------------------------------------
def kms_decrypt(ciphertext, b64=True, aws_session=None):
    """
    Decrypt a ciphertext blob that was encrypted using KMS. The following
    command generates the ciphertext:

        aws kms encrypt --key-id alias/mykey --plaintext fileb://plaintext \
            --output text --query CiphertextBlob

    :param ciphertext:  The ciphertext blob which must have been encrypted with
                        AWS KMS. Any whitespace in the ciphertext will be removed
                        as KMS encrypted text never has whitespace.
    :param b64:         If True, the ciphertext blob is base64 encoded.
                        Default True.
    :param aws_session: A boto3 Session.

    :type ciphertext:   str
    :type b64:          bool

    :return:            The plaintext.
    :rtype:             bytearray
    """

    if not aws_session:
        aws_session = boto3.Session()

    # Squeeze out any whitespace and form a single ciphertext.
    ciphertext = ''.join(ciphertext.split())

    kms_client = aws_session.client('kms')

    if b64:
        ciphertext = b64decode(ciphertext)

    return kms_client.decrypt(CiphertextBlob=ciphertext)['Plaintext']


# ------------------------------------------------------------------------------
def aws_account_id(aws_session=None):
    """
    Get the AWS account ID. This is done with a trick by retrieving the VPC
    default security group and extracting the owner ID. The result is cached
    internally.

    :param aws_session: A boto3 Session. If None a default is creaated.

    :return:            The AWS account ID
    :rtype:             str
    """

    try:
        return aws_account_id.account_id
    except AttributeError:
        pass

    if not aws_session:
        aws_session = boto3.Session()

    sg = aws_session.client('ec2').describe_security_groups(GroupNames=['default'])['SecurityGroups']
    aws_account_id.account_id = sg[0]['OwnerId']
    return aws_account_id.account_id


# ------------------------------------------------------------------------------
def s3_load_yaml(bucket_name, key, aws_session=None):
    """
    Load a YAML file from S3.

    :param bucket_name: Name of the S3 bucket.
    :param key:         Object key (file name) for the YAML file
    :param aws_session: A boto3 Session. If None a default is creaated.
    :type bucket_name:  str
    :type key:          str
    :return:            The object decoded from the YAML file.
    :rtype:             T

    :raise IOError:     If file doesn't exist or can't be retrieved.
    :raise ValueError:  If the file was retrieved but is not valid YAML.
    """

    if not aws_session:
        aws_session = boto3.Session()

    try:
        response = aws_session.resource('s3').Object(bucket_name, key).get()
    except Exception as e:
        raise IOError(str(e))

    try:
        return yaml.safe_load(response['Body'])
    except Exception as e:
        raise ValueError(str(e))


# ------------------------------------------------------------------------------
def s3_check_bucket_security(bucket_name,
                             require_no_public_access=True,
                             require_encryption=True,
                             require_server_logging=True,
                             require_bucket_is_mine=True,
                             aws_session=None):
    """
    Performs a bunch of security checks on am S3 bucket. These are described by
    the various require_* variables.


    :param bucket_name: The bucket name
    :param require_no_public_access:
                        If True the bucket must not have any public access.
                        Default True.
    :param require_encryption:
                        If True the bucket must have default encryption enabled.
                        Default True.
    :param require_server_logging:
                        If True the bucket must have default server logging
                        enabled. Default True.
    :param require_bucket_is_mine:
                        If True the bucket must belong to the current AWS
                        account. Default True.
    :param aws_session: A boto3 Session. If None a default is creaated.

    :type bucket_name:  str
    :type require_no_public_access: bool
    :type require_encryption: bool
    :type require_server_logging: bool
    :type require_bucket_is_mine: bool
    :type aws_session:  boto3.Session

    :return:            Nothing.
    :raise Exception:   If the bucket fails any of the security checks or the
                        status of any of the checks cannot be verified.

    """

    # Check bucket has no public access
    if require_no_public_access and s3_bucket_is_public(bucket_name, aws_session):
        raise Exception('Insecure due to public access')

    # Check bucket has default encryption enabled
    if require_encryption and not s3_bucket_is_encrypted(bucket_name, aws_session):
        raise Exception('Insecure due to lack of default encryption')

    # Check bucket has server logging enabled
    if require_server_logging and not s3_bucket_is_server_logging_enabled(bucket_name, aws_session):
        raise Exception('Insecure due to lack of server logging')

    # Check bucket belongs to the current AWS account
    if require_bucket_is_mine and not s3_bucket_is_mine(bucket_name, aws_session):
        raise Exception('Insecure due to foreign owned bucket')


# ------------------------------------------------------------------------------
def s3_bucket_is_server_logging_enabled(bucket_name, aws_session=None):
    """
    Check if a bucket has server logging enabled.

    :param bucket_name: The bucket name
    :param aws_session: A boto3 Session. If None a default is creaated.

    :type bucket_name:  str
    :type aws_session:  boto3.Session

    :return:            True if the bucket has logging enabled.

    :raise Exception:   If the bucket doesn't exist or logging status cannot be
                        determined.
    """

    if not aws_session:
        aws_session = boto3.Session()

    s3 = aws_session.client('s3')

    try:
        return 'LoggingEnabled' in s3.get_bucket_logging(Bucket=bucket_name)
    except Exception as e:
        raise Exception('Cannot verify bucket has logging enabled - {}'.format(e))


# ------------------------------------------------------------------------------
def s3_bucket_is_encrypted(bucket_name, aws_session=None):
    """
    Check if a bucket has default encryption enabled.

    :param bucket_name: The bucket name
    :param aws_session: A boto3 Session. If None a default is creaated.

    :type bucket_name:  str
    :type aws_session:  boto3.Session

    :return:            True if the bucket has default encryption enabled.

    :raise Exception:   If the bucket doesn't exist or encryption status cannot
                        be determined.
    """

    if not aws_session:
        aws_session = boto3.Session()

    s3 = aws_session.client('s3')

    try:
        s3.get_bucket_encryption(Bucket=bucket_name)
        return True
    except ClientError as e:
        if 'ServerSideEncryptionConfigurationNotFoundError' in e.args[0]:
            return False
        raise Exception('Cannot verify bucket is encrypted - {}'.format(e))
    except Exception as e:
        raise Exception('Cannot verify bucket is encrypted - {}'.format(e))


# ------------------------------------------------------------------------------
def s3_bucket_is_public(bucket_name, aws_session=None):
    """
    Determines if a bucket has any public visibility.

    :param bucket_name: The bucket name
    :param aws_session: A boto3 Session. If None a default is creaated.

    :type bucket_name:  str
    :type aws_session:  boto3.Session

    :return:            True if there is any public access to the bucket.

    :raise Exception:   If the bucket doesn't exist or the ACL cannot be read.
    """

    if not aws_session:
        aws_session = boto3.Session()

    bucket = aws_session.resource('s3').Bucket(bucket_name)
    acl = bucket.Acl()

    for grant in acl.grants:
        # http://docs.aws.amazon.com/AmazonS3/latest/dev/acl-overview.html
        if grant['Grantee']['Type'].lower() == 'group' \
                and grant['Grantee']['URI'] == 'http://acs.amazonaws.com/groups/global/AllUsers':
            return True

    return False


# ------------------------------------------------------------------------------
@static_vars(is_mine={})
def s3_bucket_is_mine(bucket_name, aws_session=None):
    """
    Determines if a bucket is owned by the current account. Requires IAM list
    buckets permission and access to the bucket ACL. Results are cached as
    buckets are not likely to change owning accounts.

    :param bucket_name: The bucket name
    :param aws_session: A boto3 Session. If None a default is creaated.

    :type bucket_name:  str
    :type aws_session:  boto3.Session

    :return:            True if the bucket is owned by the current account.

    :raise Exception:   If the bucket doesn't exist or the ownership cannot be
                        determined.
    """

    if not aws_session:
        aws_session = boto3.Session()

    if bucket_name not in s3_bucket_is_mine.is_mine:
        s3 = aws_session.client('s3')

        # Get the canonical ID for the current acccount
        canonical_id = s3.list_buckets()['Owner']['ID']

        bucket_owner = s3.get_bucket_acl(Bucket=bucket_name)['Owner']['ID']

        s3_bucket_is_mine.is_mine[bucket_name] = canonical_id == bucket_owner

    return s3_bucket_is_mine.is_mine[bucket_name]


# ------------------------------------------------------------------------------
def s3_search_yaml(bucket_name, prefix, filename, aws_session=None):
    """
    Load a YAML file from S3, recursing up the prefix hierarchy if necessary to
    find the file.

    i.e. If the prefix is specified as a/b/c and the filename is z.yaml, then
    the first of the following to exist will be loaded:
        - a/b/c/z.yaml
        - a/b/z.yaml
        - a/z.yaml
        - z.yaml

    :param bucket_name: Name of the S3 bucket.
    :param prefix:      The search path. Do NOT end with /
    :param filename:    The YAML file basename.
    :param aws_session: A boto3 Session. If None a default is creaated.
    :type bucket_name:  str
    :type prefix:       str
    :type filename:     str
    :return:            A tuple (loaded object key, loaded object).
    :rtype:             (str, T)

    :raise IOError:     If file doesn't exist or can't be retrieved.
    :raise ValueError:  If the file was retrieved but is not valid YAML.
    """

    pfx_list = prefix.split('/')

    # Recurse up the tree
    for n in range(len(pfx_list), -1, -1):
        key = sepjoin('/', pfx_list[:n], filename)
        try:
            return key, s3_load_yaml(bucket_name, key, aws_session)
        except IOError:
            pass

    raise IOError('Could not find {} in {}/{}'.format(filename, bucket_name, prefix))


# ------------------------------------------------------------------------------
def s3_load_json(bucket_name, key, aws_session=None):
    """
    Load a JSON file from S3.

    :param bucket_name: Name of the S3 bucket.
    :param key:         Object key (file name) for the JSON file
    :param aws_session: A boto3 Session. If None a default is creaated.
    :type bucket_name:  str
    :type key:          str
    :return:            The object decoded from the JSON file.
    :rtype:             T

    :raise IOError:     If file doesn't exist or can't be retrieved.
    :raise ValueError:  If the file was retrieved but is not valid JSON.
    """

    if not aws_session:
        aws_session = boto3.Session()

    try:
        response = aws_session.resource('s3').Object(bucket_name, key).get()
    except Exception as e:
        raise IOError(str(e))

    try:
        return json.load(response['Body'])
    except Exception as e:
        raise ValueError('Bad JSON: {}'.format(str(e)))


# ------------------------------------------------------------------------------
def s3_split(s):
    """
    Split an S3 object name into bucket and prefix components.

    :param s:       The object name. Typically bucket/prefix but the following
                    are also accepted:
                        s3:bucket/prefix
                        s3://bucket/prefix
                        /bucket/prefix

    :type s:        str
    :return:        A tuple (bucket, prefix)
    :rtype:         tuple(str, str)
    """

    # Clean off any s3:// type prefix
    for p in 's3://', 's3:':
        if s.startswith(p):
            s = s[len(p):]
            break

    t = s.strip('/').split('/', 1)

    if not t[0]:
        raise ValueError('Invalid S3 object name: {}'.format(s))
    return t[0], t[1].strip('/') if len(t) > 1 else ''


# ..............................................................................
# endregion S3 utils.
# ..............................................................................


# ..............................................................................
# region Redshift utils
# ..............................................................................


# ------------------------------------------------------------------------------
def redshift_table_exists(cursor, schema, table):
    """
    Check if a Redshift table exists (as far as the user owning the connection
    can tell).

    :param cursor:      Database cursor
    :param schema:      Schema name
    :param table:       Table name

    :type schema:       str
    :type table:        str

    :return:            True if table exists, False otherwise
    :rtype:             bool
    """

    cursor.execute(
        """
        SELECT EXISTS (
            SELECT 1 FROM information_schema.tables
            WHERE  table_schema = '{}' AND table_name = '{}'
        );
        """.format(schema, table)
    )
    return cursor.fetchone()[0]


# ------------------------------------------------------------------------------
def redshift_relations_in_schema(cursor, schema, tables=True, views=True):
    """
    List the relations (tables and views) in the specified schema.

    :param cursor:      Database cursor
    :param schema:      Schema name
    :param tables:      If True include tables. Default True.
    :param views:       If True include views. Default True.

    :type schema:       str
    :type tables:       bool
    :type views:        bool

    :return:            A list of relation names (without schema).
    :rtype:             list[str]
    """

    where = []
    # This version seems to suffer intermittently from the
    #       "Relation with OID nnnn does not exist"
    # problem.
    # ----------------------------------------
    # if tables:
    #     where.append('BASE TABLE')
    # if views:
    #     where.append('VIEW')
    #
    # if not where:
    #     raise ValueError('At least one of tables or views must be included')
    #
    # sql = """
    #         SELECT table_name FROM information_schema.tables
    #         WHERE table_schema = %s AND table_type in ({});
    #     """.format(','.join("'" + w + "'" for w in where))
    # ----------------------------------------

    if tables:
        where.append('r')
    if views:
        where.append('v')

    if not where:
        raise ValueError('At least one of tables or views must be included')

    sql = """
            SELECT c.relname FROM pg_catalog.pg_class c
              LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
            WHERE c.relkind in ({}) and n.nspname = %s
            ORDER BY c.relname;
        """.format(','.join("'" + w + "'" for w in where))

    cursor.execute(sql, (schema,))

    return [row[0] for row in cursor.fetchall() if row]


# ------------------------------------------------------------------------------
def redshift_oid(cursor, schema, relation):
    """
    Get the OID for the specified relation.

    :param cursor:      Database cursor.
    :param schema:      Schema name.
    :param relation:    Relation (table or view) name.

    :type schema:       str
    :type relation:     str

    :return:            The OID of the object or None if the object doesn't
                        exist.
    :rtype:             int
    """

    sql = """
            SELECT c.oid from pg_catalog.pg_class c
            LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
            WHERE n.nspname = %s AND c.relname = %s;
    """

    cursor.execute(sql, (schema.lower(), relation.lower()))
    result = cursor.fetchall()
    return int(result[0][0]) if result else None


# ------------------------------------------------------------------------------
def redshift_get_column_info(cursor, schema, relation):
    """
    Get the column information for the specified relation.

    :param cursor:      Database cursor.
    :param schema:      Schema name.
    :param relation:    Relation (table or view) name.

    :type schema:       str
    :type relation:     str

    :return:            A list of tuples (column name, type) with all components
                        guaranteed to be in lower case.

    :raise Exception:   If the relation doesn't exist.

    """

    oid = redshift_oid(cursor, schema, relation)

    if oid is None:
        raise Exception('Cannot get column info for {}.{}'.format(
            schema, relation))

    sql = """
        SELECT LOWER(a.attname),
               LOWER(pg_catalog.format_type(a.atttypid, a.atttypmod))
        FROM pg_catalog.pg_attribute a
        WHERE a.attrelid = %s AND a.attnum > 0 AND NOT a.attisdropped
        ORDER BY a.attnum;
    """

    cursor.execute(sql, (oid,))
    return [(row[0], row[1]) for row in cursor.fetchall() if row]

# ..............................................................................
# endregion Redshift utils
# ..............................................................................


# ------------------------------------------------------------------------------
def sns_get_topic_arn_by_name(topic, aws_session=None):
    """
    Find a topic ARN with the given name in the current account.
    
    :param topic:       The topic name
    :param aws_session: A boto3 Session. If None a default is creaated.
    
    :return:            The topic ARN or None if not found.
    :rtype:             str
    
    """

    if not aws_session:
        aws_session = boto3.Session()

    sns = aws_session.client('sns')

    for page in sns.get_paginator('list_topics').paginate():
        for t in page['Topics']:  # type: dict
            if t['TopicArn'].endswith(':' + topic):
                return t['TopicArn']

    return None


# ------------------------------------------------------------------------------
def get_ssm_param(name, decrypt=True, aws_session=None):
    """
    This function reads a secure parameter from AWS SSM service.

    Warning: Does not handle list of string parameters sensibly.

    :param name:        Valid SSM parameter name
    :param decrypt:     If True, decrypt parameter values. Default True.
    :param aws_session: A boto3 Session. If None a default is creaated.

    :type name:         str
    :type decrypt:      bool

    :return:            The parameter value.
    :rtype:             str

    :raise Exception:   If the parameter doesn't exist or cannot be accessed.

    """

    if not aws_session:
        aws_session = boto3.Session()

    ssm = aws_session.client('ssm')
    response = ssm.get_parameter(Name=name, WithDecryption=decrypt)
    return response['Parameter']['Value']
