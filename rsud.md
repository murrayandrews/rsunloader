# rsud(1) -- Redshift Unloader (SQS)


## SYNOPSIS

`rsud` \[options\] control-file ...

## DESCRIPTION

**Rsud** automates the process of unloading data from Redshift to AWS S3.  It is
a multi-threaded daemon and is designed to simplify the process of unloading
lots of tables and views on demand.

**Rsud** differs from its counterpart **rsu** in that it reads unload requests
from an AWS SQS queue rather than from a control file. A control file is still
required to specify other parameters for the unloading process.

## OPTIONS

*   _control-file_:
    A YAML formatted control file that species parameters that control the
    unloading process. The format is very similar to the control format used by
    **rsu**.  A control file name starting with _s3://_ indicates the file is to
    be downloaded from S3.

*   `-b` _HEARTBEAT_, `--heartbeat` _HEARTBEAT_:
    Emit a heartbeat log message (log level _info_) every this many seconds.  A
    value of 0 (the default) disables heartbeats. If specified, a minimum of 30
    seconds is imposed. The heartbeat message contains the current, estimated,
    SQS queue length, the number of non-visible messages and the length of the
    internal event processing queue.

*   `--batch`:
    Process messages until the SQS queue is empty and then exit. Mutually
    exclusive with the `-d`, `--daemon` option. If neither option is used,
    **rsud** runs continuously in the foreground.

*   `-c`, `--no-colour`, `--no-color`:
    Don't use colour in information messages.

*   `--cluster-table` _CLUSTER-TABLE_:
    **Rsud** reads connectivity information for Redshift clusters from a
    DynamoDB table. This option specifies the name of the DynamoDB table. If not
    specified the value of the <RSUNLOAD_CLUSTER_TABLE> environment variable is
    used. If this is not set, the default is <clusters>.

*   `--dry-run`:
    Do a dry run. No unloads are performed but relations that would be unloaded
    are identified.

*   `-d`, `--daemon`:
    Run as a daemon. The process ID will be written to the file specified with
    the `--lock-file` argument to prevent two daemons running at the same time.
    Mutually exclusive with the `--batch` option. If neither option is used,
    **rsud** runs in the foreground.

*   `-h`:
    Print help and exit.

*   `--insecure`:
    Allow writing to insecure S3 buckets. By default, some BASIC security
    checks are performed on the bucket to reduce the risk of accidents but DO
    NOT rely on this for security.


*   `-l` _LEVEL_, `--level` _LEVEL_:
    Print messages of a given severity level or above. The standard logging
    level names are available but _debug_, _info_, _warning_ and _error_ are
    most useful.  The Default is _info_.

*   `--lock-file` _LOCK-FILE_:
    Create a lock file with the given name to prevent two instances using the
    same lock file from running simultaneously.

*   `--log` _LOG_:
    Log to the specified target. This can be either a file name or a syslog
    facility with an @ prefix (e.g. _@local0_).

*   `--profile` _profile_:
    As for the AWS CLI **--profile** option.

*   `-q` _QUEUE_, `--queue` _QUEUE_:
    AWS SQS queue name. If specified, overrides any value in the control file.
    The queue name must be specified either in the control file or on the
    command line.

*   `-r` _RETRIES_, `--retries` _RETRIES_:
    Maximum number of retry attempts to process an event. Default 2. If an
    unload request is not processed after this many retries, it is discarded. If
    a negative number is specified, retry limiting is disabled. A retry can
    occur either because **rsud** could not tell AWS SQS that it had finished
    processing it, or if the unload processing experienced an error that might
    be overcome by retrying. In either case, the retry will occur after the SQS
    queue visibility timeout has expired. If the unload processing experiences a
    permanent failure it will not be retried.  It is possible for an unload
    request to be processed more than once if the processing was completed but
    **rsud** could not signal completion to SQS.

*   `-s` _SLEEP_, `--sleep` _SLEEP_:
    Sleep for this many seconds between SQS poll attempts when no messages are
    available.  Default 10.  Note that the `-w`/`--wait` time is outside this
    sleep time.

*   `-t` _THREADS_, `--threads` _THREADS_:
    Run this many threads. Default 2.

*   `--tag` _TAG_:
    Tag log entries with the specified value. The default is <rsud>.

*   `-v`, `--version`:
    Print the version and exit.

*   `-w` _WAIT_, `--wait` _WAIT_:
    Wait this many seconds for the SQS queue to provide messages (long polling).
    Must be in the range 0 .. 20.  Default is 10 seconds.


## ENVIRONMENT

*   <RSUNLOAD_CLUSTER_TABLE>:
    Optionally specifies the name of the DynamoDB table containing
    cluster connectivity information.


## SEE ALSO

[rsunloader](https://bitbucket.org/murrayandrews/rsunloader) on Bitbucket.

rsu(1)

## AUTHOR

Murray Andrews

## LICENCE

[BSD 3-clause licence](http://opensource.org/licenses/BSD-3-Clause).
